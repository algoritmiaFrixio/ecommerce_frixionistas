import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperRootComponent } from './super-root.component';

describe('SuperRootComponent', () => {
  let component: SuperRootComponent;
  let fixture: ComponentFixture<SuperRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
