import {Routes} from '@angular/router';

import {MainComponent} from './main/main.component';
import {SuperRootComponent} from './super-root/super-root.component';

export const rootRouterConfig: Routes = [
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: 'admin',
    component: SuperRootComponent,
    children: [
      {
        path: '',
        loadChildren: './staff/staff.module#StaffModule'
      }
    ]
  },
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'inicio',
        loadChildren: './shop/shop.module#ShopModule'
      },
      {
        path: 'paginas',
        loadChildren: './pages/pages.module#PagesModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'inicio'
  }
];

