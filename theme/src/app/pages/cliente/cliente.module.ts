import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InicioComponent} from './inicio/inicio.component';
import {ClienteRoutingModule} from './cliente-routing.module';
import {RouterModule} from '@angular/router';
import {PasswordComponent} from './password/password.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AccountComponent } from './account/account.component';
import { WhishlistComponent } from './whishlist/whishlist.component';
import { OrdersComponent } from './orders/orders.component';

@NgModule({
  imports: [
    CommonModule,
    ClienteRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [InicioComponent, PasswordComponent, AccountComponent, WhishlistComponent, OrdersComponent]
})
export class ClienteModule {
}
