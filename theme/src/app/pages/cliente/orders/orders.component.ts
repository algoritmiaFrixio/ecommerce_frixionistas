import {Component, OnInit} from '@angular/core';
import {Pedidos} from '../../../staff/interfaces/pedidos/pedidos';
import {Products} from '../../../staff/interfaces/pedidos/products';
import {PedidosService} from '../../../shared/services/pedidos.service';
import {Login} from '../../../shared/classes/login/login';
import {ProductsService} from '../../../shared/services/products.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  public orders: Pedidos[] = [];
  public data: Login;

  constructor(private orderService: PedidosService, public productsService: ProductsService) {
  }

  ngOnInit() {
    this.data = JSON.parse(atob(localStorage.getItem(btoa('auth'))));
    this.orderService.getOrders(this.data.cliente.id).subscribe((response: Pedidos[]) => {
      this.orders = response;
    }, error1 => {
      console.error(error1);
    });
  }

  public getTotal(mult: Products[]): number {
    return this.orderService.getTotalAmount(mult);
  }

  public getGreatAmount(item: Pedidos[]): number {
    return this.orderService.getGreatTotal(item);
  }
}
