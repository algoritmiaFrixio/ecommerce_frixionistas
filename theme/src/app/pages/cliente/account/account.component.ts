import {Component, OnInit} from '@angular/core';
import {Login} from '../../../shared/classes/login/login';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../shared/services/login.service';
import {ToastrService} from 'ngx-toastr';
import {OrderService} from '../../../shared/services/order.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  public data: Login;
  public checkoutForm: FormGroup;
  public paises;

  constructor(private fb: FormBuilder, private loginService: LoginService, private toastr: ToastrService, private orderService: OrderService) {
    this.data = JSON.parse(atob(localStorage.getItem(btoa('auth'))));
    const tmp = this.data.cliente.buyerFullName.split(' ');
    let firstname = '';
    let lastname = '';
    switch (tmp.length) {
      case 2:
        firstname += tmp[1];
        lastname += tmp[0];
        break;
      case 3:
        firstname += tmp[2];
        lastname += tmp[0] + ' ';
        lastname += tmp[1];
        break;
      case 4:
        firstname += tmp[2] + ' ';
        firstname += tmp[3];
        lastname += tmp[0] + ' ';
        lastname += tmp[1];
        break;
      case 5:
        firstname += tmp[2] + ' ';
        firstname += tmp[3] + ' ';
        firstname += tmp[4];
        lastname += tmp[0] + ' ';
        lastname += tmp[1];
    }
    this.checkoutForm = this.fb.group({
      firstname: [firstname, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      lastname: [lastname, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      telephone: [this.data.cliente.telephone, [Validators.required, Validators.pattern('[0-9]+')]],
      buyerEmail: [this.data.cliente.buyerEmail, [Validators.email]],
      shippingAddress: [this.data.cliente.shippingAddress, [Validators.required, Validators.maxLength(50)]],
      shippingCountry: [this.data.cliente.shippingCountry, Validators.required],
      shippingCity: [this.data.cliente.shippingCity, Validators.required],
      state: [this.data.cliente.state, Validators.required],
      postalcode: [this.data.cliente.postalcode]
    });
  }

  ngOnInit() {
    this.orderService.getCountries().subscribe(response => {
      this.paises = response;
    });
  }

  public update(form: FormGroup) {
    this.loginService.updateAccount(form.value, this.data.cliente.id).subscribe((response: Login) => {
      this.data = response;
      localStorage.setItem(btoa('auth'), btoa(JSON.stringify(response)));
      const tmp = this.data.cliente.buyerFullName.split(' ');
      let firstname = '';
      let lastname = '';
      switch (tmp.length) {
        case 2:
          firstname += tmp[0];
          lastname += tmp[1];
          break;
        case 3:
          firstname += tmp[0];
          lastname += tmp[1] + ' ';
          lastname += tmp[2];
          break;
        case 4:
          firstname += tmp[0] + ' ';
          firstname += tmp[1];
          lastname += tmp[2] + ' ';
          lastname += tmp[3];
      }
      this.checkoutForm.controls['firstname'].setValue(firstname);
      this.checkoutForm.controls['lastname'].setValue(lastname);
      this.checkoutForm.controls['telephone'].setValue(this.data.cliente.telephone);
      this.checkoutForm.controls['buyerEmail'].setValue(this.data.cliente.buyerEmail);
      this.checkoutForm.controls['shippingAddress'].setValue(this.data.cliente.shippingAddress);
      this.checkoutForm.controls['shippingCountry'].setValue(this.data.cliente.shippingCountry);
      this.checkoutForm.controls['shippingCity'].setValue(this.data.cliente.shippingCity);
      this.checkoutForm.controls['state'].setValue(this.data.cliente.state);
      this.checkoutForm.controls['postalcode'].setValue(this.data.cliente.postalcode);
      this.toastr.success('Actualizado correctamente');
    }, error => {
      console.error(error);
      this.toastr.error(error.error.message);
    });
  }

}
