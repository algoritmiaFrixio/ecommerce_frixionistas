import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InicioComponent} from './inicio/inicio.component';
import {PasswordComponent} from './password/password.component';
import {AccountComponent} from './account/account.component';
import {WhishlistComponent} from './whishlist/whishlist.component';
import {OrdersComponent} from './orders/orders.component';

const routes: Routes = [
  {
    path: 'info',
    component: InicioComponent
  },
  {
    path: 'cambiar-contraseña',
    component: PasswordComponent
  },
  {
    path: 'cuenta',
    component: AccountComponent
  },
  {
    path: 'lista',
    component: WhishlistComponent
  },
  {
    path: 'pedidos',
    component: OrdersComponent
  },
  {
    path: '**',
    redirectTo: 'info'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule {
}

