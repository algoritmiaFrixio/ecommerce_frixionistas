import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {
  public changeForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.changeForm = this.fb.group({
      password: ['', Validators.required],
      password2: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

}
