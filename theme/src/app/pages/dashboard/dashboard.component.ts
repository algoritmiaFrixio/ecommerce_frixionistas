import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public url: any;
  constructor(private router: Router) {
    if (localStorage.getItem(btoa('auth')) === null) {
      this.router.navigate(['/paginas/login']);
    }
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.url = event.url;
      }
    });
  }

  ngOnInit() {
    $.getScript('assets/js/script.js');
  }

}
