import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AboutUsComponent} from './about-us/about-us.component';
import {ErrorPageComponent} from './error-page/error-page.component';
import {LoginComponent} from './login/login.component';
import {CollectionComponent} from './collection/collection.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {ContactComponent} from './contact/contact.component';
import {DashboardComponent} from './dashboard/dashboard.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'quienes-somos',
        component: AboutUsComponent
      },
      {
        path: '404',
        component: ErrorPageComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      // {
      //   path: 'register',
      //   component: RegisterComponent
      // },
      {
        path: 'coleccion',
        component: CollectionComponent
      },
      {
        path: 'olvido',
        component: ForgetPasswordComponent
      },
      {
        path: 'contacto',
        component: ContactComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        children: [
          {
            path: '',
            loadChildren: './cliente/cliente.module#ClienteModule'
          }
        ]
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
