import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../shared/services/login.service';
import {Login} from '../../shared/classes/login/login';
import {ToastrService} from 'ngx-toastr';
import {ProductsService} from '../../shared/services/products.service';
import {Router} from '@angular/router';
import {TopbarComponent} from '../../shared/header/widgets/topbar/topbar.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public formLogin: FormGroup;

  constructor( private _LoginService: LoginService,
              private toastr: ToastrService, private ProductService: ProductsService, private router: Router) {
    if (localStorage.getItem(btoa('auth')) !== null) {
      this.router.navigate(['/paginas/dashboard']);
    }
    this.formLogin = new FormGroup({
      cedula: new FormControl('', Validators.required),
      nombre: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  public login(form: FormGroup) {
    console.log(form.value)
    this._LoginService.login(form.value).subscribe(
      (response: Login) => {
        if (response.frixionista) {
          this.ProductService.currency = 'MAY';
        }
        localStorage.setItem(btoa('auth'), btoa(JSON.stringify(response)));
        this._LoginService.setData();
        this.router.navigate(['/inicio']);
      },
      error1 => {
        this.toastr.error(error1.error.message);
        console.error(error1);
      }
    );
  }

  public fileEvent($event) {
    const fileSelected: File = $event.target.files[0];
    this._LoginService.uploadFile(fileSelected)
      .subscribe((response) => {
          console.log(response);
        },
        error => {
          console.error(error);
        });
  }
}
