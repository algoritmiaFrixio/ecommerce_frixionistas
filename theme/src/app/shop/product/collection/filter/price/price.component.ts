import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PriceComponent implements OnInit {

  // Using Output EventEmitter
  @Output() priceFilters: EventEmitter<number[]> = new EventEmitter<number[]>();
  @Input() maxAndMin: number[];
  // define min, max and range
  public min = 10;
  public max = 1000;
  public range = [10, 1000];

  constructor() {
  }

  ngOnInit() {
    this.min = this.getMin(this.maxAndMin);
    this.max = this.getMax(this.maxAndMin);
    this.range = [this.min, this.max];
  }

  private getMin(range: number[]): number {
    let tmp = 0;
    range.forEach((e, i) => {
      if (i === 0) {
        tmp = e;
      } else {
        tmp = e <= tmp ? e : tmp;
      }
    });
    return tmp;
  }

  private getMax(range: number[]): number {
    let tmp = 0;
    range.forEach((e, i) => {
      if (i === 0) {
        tmp = e;
      } else {
        tmp = e >= tmp ? e : tmp;
      }
    });
    return tmp;
  }
  priceChanged(event: any) {
    this.priceFilters.emit(event);
  }

  // // rangeChanged
  // priceChanged(event:any) {
  //   setInterval(() => {
  //     this.priceFilters.emit(event);
  //   }, 1000);
  // }

}
