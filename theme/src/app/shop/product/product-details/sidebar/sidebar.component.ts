import {Component, OnInit} from '@angular/core';
import {LoginService} from "../../../../shared/services/login.service";

@Component({
  selector: 'product-details-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(public LoginService: LoginService) {
  }

  ngOnInit() {
  }

}
