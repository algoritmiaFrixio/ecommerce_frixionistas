import {Component, OnInit} from '@angular/core';
import {Product} from '../../../../shared/classes/product';
import {ProductsService} from '../../../../shared/services/products.service';
import {CartService} from "../../../../shared/services/cart.service";

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  public products: Product[] = [];

  constructor(public productsService: ProductsService, private cartService: CartService) {
  }

  ngOnInit() {
    this.productsService.getProducts().subscribe(product => {
      const number2 = Math.floor(Math.random() * product.length);
      if ((number2 + 3) > product.length) {
        this.products = product.slice(number2 - 3, number2);
      } else {
        this.products = product.slice(number2, number2 + 3);
      }
    });
  }

}
