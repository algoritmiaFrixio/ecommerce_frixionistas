import {Compiler, Component, OnInit} from '@angular/core';
import {Order} from '../../../shared/classes/order';
import {OrderService} from '../../../shared/services/order.service';
import {ActivatedRoute} from '@angular/router';
import {CartService} from '../../../shared/services/cart.service';
import {BehaviorSubject} from "rxjs";
import {ProductsService} from "../../../shared/services/products.service";

@Component({
  selector: 'app-order-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

  public orderDetails: Order = {};
  public state;
  public code;
  public entidad;
  public msgState;
  public method;
  public porcdtco: number = 0;
  public fEntrega = new Date();
  public data = new Date();
  public options = {year: 'numeric', month: 'long', day: 'numeric'};

  constructor(public productsService: ProductsService, private orderService: OrderService, private compiler: Compiler, private route: ActivatedRoute, private cartService: CartService) {
    this.orderDetails = localStorage.getItem(btoa('item')) !== null ? JSON.parse(atob(localStorage.getItem(btoa('item')))) : null;
    this.fEntrega.setDate(this.fEntrega.getDate() + 12);
    localStorage.removeItem(btoa('cart'));
    localStorage.removeItem(btoa('item'));
    this.cartService.product = [];
    this.cartService.cartItems = new BehaviorSubject([]);
    let quant = 0;
    this.orderDetails.product.forEach(e => {
      quant += e.quantity;
    })
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.state = +params['state'];
      this.code = +params['code'];
      this.entidad = params['entidad'];
      this.method = params['method'];
      if (this.state == 4) {
        this.msgState = "confirmado";
      } else if (this.state == 6) {
        this.msgState = "cancelado";
      } else if (this.state == 7) {
        this.msgState = "pendiente";
      }
      this.orderService.confirmartion({
        reference_sale: this.code,
        payment_method_type: this.method,
        state_pol: this.state,
        payment_method_name: this.entidad
      }).subscribe(response => {
        console.log(response);
      }, error1 => {
        console.error(error1);
      });
    });
  }
}
