import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {CollectionLeftSidebarComponent} from './product/collection/collection-left-sidebar/collection-left-sidebar.component';
import {ProductLeftSidebarComponent} from './product/product-details/product-left-sidebar/product-left-sidebar.component';
import {SearchComponent} from './product/search/search.component';
import {WishlistComponent} from './product/wishlist/wishlist.component';
import {ProductCompareComponent} from './product/product-compare/product-compare.component';
import {CartComponent} from './product/cart/cart.component';
import {CheckoutComponent} from './product/checkout/checkout.component';
import {SuccessComponent} from './product/success/success.component';

// Routes
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'categoria/:category',
    component: CollectionLeftSidebarComponent
  },
  {
    path: 'product/:id',
    component: ProductLeftSidebarComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'lista',
    component: WishlistComponent
  },
  {
    path: 'comparar',
    component: ProductCompareComponent
  },
  {
    path: 'carrito',
    component: CartComponent
  },
  {
    path: 'comprar',
    component: CheckoutComponent
  },
  {
    path: 'comprar/estado/:state/:code/:entidad/:method',
    component: SuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule {
}
