import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-parallax-banner',
  templateUrl: './parallax-banner.component.html',
  styleUrls: ['./parallax-banner.component.scss']
})
export class ParallaxBannerComponent implements OnInit {
  today: number = Date.now();

  constructor() {
  }

  ngOnInit() {
  }

}
