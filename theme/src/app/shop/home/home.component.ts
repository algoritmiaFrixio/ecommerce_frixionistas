import {Component, OnInit} from '@angular/core';
import {Product} from '../../shared/classes/product';
import {ProductsService} from '../../shared/services/products.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public products: Product[] = [];
  public sliderProducts: Product[] = [];

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.productsService.getProducts().subscribe(product => {
      const number = Math.floor(Math.random() * product.length);
      const number2 = Math.floor(Math.random() * product.length);
      if ((number + 8) > product.length) {
        this.sliderProducts = product.slice(number - 8, number);
      } else {
        this.sliderProducts = product.slice(number, number + 8);
      }
      if ((number2 + 8) > product.length) {
        this.products = product.slice(number2 - 8, number2);
      } else {
        this.products = product.slice(number2, number2 + 8);
      }
    });
  }


}
