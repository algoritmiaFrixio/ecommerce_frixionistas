import {Component, OnInit} from '@angular/core';
import {PedidosService} from '../../../services/pedidos/pedidos.service';
import {ActivatedRoute} from '@angular/router';
import {Pedidos} from '../../../interfaces/pedidos/pedidos';
import {Products} from '../../../interfaces/pedidos/products';
import {ProductsService} from '../../../../shared/services/products.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-view-id',
  templateUrl: './view-id.component.html',
  styleUrls: ['./view-id.component.scss']
})
export class ViewIdComponent implements OnInit {
  public order: Pedidos;
  public total = 0;
  public fEntrega;
  public status = '';
  public options = { year: 'numeric', month: 'long', day: 'numeric' };
  constructor(private OrderService: PedidosService, private route: ActivatedRoute, public productsService: ProductsService, private toastr: ToastrService) {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.OrderService.getOrder(id).subscribe((pedido: Pedidos) => {
        this.order = pedido;
        this.fEntrega = new Date(this.order.created_at);
        this.fEntrega.setDate(this.fEntrega.getDate() + 12);
        this.status = this.order.status;
      });
    });
  }

  ngOnInit() {
  }

  changeStatus(id) {
    this.OrderService.changeStatus(id, this.status).subscribe((response: boolean) => {
      this.toastr.success('Actualizado correctamente');
    }, error1 => {
      console.error(error1);
      this.toastr.error(error1.error.message);
    });
  }

  public getTotal(mult: Products[]): number {
    return this.OrderService.getTotalAmount(mult);
  }
}
