import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ViewComponent} from './view/view.component';
import {ViewIdComponent} from './view-id/view-id.component';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent
  },
  {
    path: 'view/:id',
    component: ViewIdComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PedidosRoutingModule {
}
