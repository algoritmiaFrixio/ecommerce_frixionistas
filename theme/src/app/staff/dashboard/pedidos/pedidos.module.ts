import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PedidosRoutingModule} from './pedidos-routing.module';
import {ViewComponent} from './view/view.component';
import {ViewIdComponent} from './view-id/view-id.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PedidosRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [ViewComponent, ViewIdComponent]
})
export class PedidosModule {
}
