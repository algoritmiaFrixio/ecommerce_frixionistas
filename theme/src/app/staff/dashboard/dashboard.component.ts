import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private route: Router) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('user') === null) {
      this.route.navigate(['home']);
    }
  }

}
