import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StaffRoutingModule} from './staff-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {ProductsComponent} from './dashboard/products/products.component';
import {PedidosComponent} from './dashboard/pedidos/pedidos.component';
import {GraphicalComponent} from './dashboard/graphical/graphical.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PedidosModule} from './dashboard/pedidos/pedidos.module';
import {NgbModalModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  imports: [
    CommonModule,
    StaffRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    PedidosModule,
    NgbModalModule
  ],
  declarations: [DashboardComponent, LoginComponent, ProductsComponent, PedidosComponent, GraphicalComponent]
})
export class StaffModule {
}
