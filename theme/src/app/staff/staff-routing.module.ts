import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {ProductsComponent} from './dashboard/products/products.component';
import {PedidosComponent} from './dashboard/pedidos/pedidos.component';
import {GraphicalComponent} from './dashboard/graphical/graphical.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: 'products',
        component: ProductsComponent
      },
      {
        path: 'pedidos',
        component: PedidosComponent,
        loadChildren: './dashboard/pedidos/pedidos.module#PedidosModule'
      },
      {
        path: 'graphics',
        component: GraphicalComponent
      },
      {
        path: '**',
        redirectTo: 'products'
      }
    ]
  },
  {
    path: '',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffRoutingModule {
}

