import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Categories} from '../../../shared/classes/categories';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {Sizes} from '../../interfaces/products/sizes';
import {Color} from '../../interfaces/color/color';
import {Collection} from '../../interfaces/products/collection';
import {Products} from "../../interfaces/products/products";
import {DeleteProducts} from "../../interfaces/products/delete-products";
import {UpdateProducts} from "../../interfaces/products/update-products";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) {
  }

  public getCategories(): Observable<Categories[]> {
    return this.http.get<Categories[]>(`${environment.url}categories`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Categories[]>) => response.body));
  }

  public getSizes(): Observable<Sizes[]> {
    return this.http.get<Sizes[]>(`${environment.url}sizes`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Sizes[]>) => response.body));
  }

  public getCollection(): Observable<Collection[]> {
    return this.http.get<Collection[]>(`${environment.url}collections`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Collection[]>) => response.body));
  }

  public getColor(): Observable<Color[]> {
    return this.http.get<Color[]>(`${environment.url}colors`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Color[]>) => response.body));
  }

  public getProduct(): Observable<Products[]> {
    return this.http.get<Products[]>(`${environment.url}product`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Products[]>) => response.body));
  }

  public deleteProduct(id: number): Observable<DeleteProducts> {
    return this.http.delete<DeleteProducts>(`${environment.url}products/${id}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<DeleteProducts>) => response.body));
  }

  public updateProduct(id: number, product): Observable<UpdateProducts> {
    return this.http.put<UpdateProducts>(`${environment.url}products/${id}`, product, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<UpdateProducts>) => response.body));
  }

  public addProduct(info, images, variants) {
    const _form = new FormData();
    const price = info.price.split(',');
    _form.append('name', info.name);
    _form.append('discount', info.discount);
    _form.append('stock', info.stock);
    _form.append('tela', info.tela);
    _form.append('tecnica', info.tecnica);
    _form.append('price', price);
    _form.append('shortDetails', info.shortDetails);
    _form.append('id_subcategory', info.id_subcategory);
    _form.append('id_collection', info.id_collection);
    _form.append('sizes', info.sizes);
    _form.append('sale', info.sale);
    _form.append('new', info.new);
    _form.append('description', info.description);
    _form.append('variants', variants);
    for (let i = 0; i < images.length; i++) {
      _form.append('files' + i, images[i]);
    }
    return this.http.post(`${environment.url}products`, _form, {
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public uploadFile(fileToUpload: File) {
    const _formData = new FormData();
    _formData.append('excel', fileToUpload, fileToUpload.name);
    return this.http.post(`${environment.url}moreProducts`, _formData);
  }
}
