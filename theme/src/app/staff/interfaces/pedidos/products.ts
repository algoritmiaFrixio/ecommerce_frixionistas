import {Pictures} from './pictures';

export interface Products {
  description?: string;
  discount?: number;
  id?: number;
  id_subcategory?: number;
  name?: string;
  new?: boolean;
  quantity?: number;
  price?: number;
  sale?: boolean;
  shortDetails?: string;
  size?: string;
  stock?: number;
  pictures?: Pictures[];
  tecnica?: string;
  tela?: string;
}
