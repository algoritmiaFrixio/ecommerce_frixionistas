import {Pago} from './pago';
import {Products} from './products';

export interface Pedidos {
  id?: number;
  created_at?: string;
  entidad?: string;
  estado?: string;
  id_cliente?: number;
  method?: string;
  cantidad?: number;
  status?: string;
  name?:string;
  pago?: Pago;
  products?: Products[];
}
