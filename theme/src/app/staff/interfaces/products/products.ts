export interface Products {
  colorSpanish: string;
  description: string;
  discount: number;
  id: number;
  id_collection: number;
  id_subcategory: number;
  name: string;
  new: boolean;
  references: string;
  sale: boolean;
  shortDetails: string;
  sizes: PivotZize[];
  updated_at: string;
  stock: number;
  tecnica: string;
  tela: string;
  prices: Pivotprice[];
  sub_category: SubCategory,
  collection: { name: string }
}

export interface SubCategory {
  id: number;
  id_category: number;
  name: string;
}

export interface PivotZize {
  created_at: string;
  id_product: number;
  id_size: number;
  sizes: size;
}

export interface Pivotprice {
  created_at: string;
  id: number;
  id_price: number;
  id_product: number;
  price: number
  updated_at: string;
}

export interface size {
  id: number;
  name: string;
}
