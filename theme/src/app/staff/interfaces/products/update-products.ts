import {Products} from "./products";

export interface UpdateProducts {
  message: string;
  products: Products[]
}
