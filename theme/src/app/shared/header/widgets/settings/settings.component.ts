import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CartItem} from '../../../classes/cart-item';
import {CartService} from '../../../services/cart.service';
import {ProductsService} from '../../../services/products.service';

@Component({
  selector: 'app-header-widgets',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  @Input() shoppingCartItems: CartItem[] = [];
  public show = false;

  constructor(private translate: TranslateService,
              private cartService: CartService, public productsService: ProductsService) {
  }

  ngOnInit() {

  }

  public updateCurrency(curr) {
    this.productsService.currency = curr;
  }

  public changeLanguage(lang) {
    this.translate.use(lang);
  }

  public getTotal(mult) {
    return this.cartService.getTotal(mult);
  }

  public removeItem(item: CartItem) {
    this.cartService.removeFromCart(item);
  }

}
