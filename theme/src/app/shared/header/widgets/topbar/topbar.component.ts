import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../../../services/products.service';
import {Router} from '@angular/router';
import {LoginService} from "../../../services/login.service";

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {


  constructor(private ProductService: ProductsService, private router: Router, public LoginService: LoginService) {
  }

  ngOnInit() {
  }
}
