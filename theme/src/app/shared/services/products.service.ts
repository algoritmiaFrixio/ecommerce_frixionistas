import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Product} from '../classes/product';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import 'rxjs/add/operator/map';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Login} from '../classes/login/login';

@Injectable()

export class ProductsService {

  public currency = 'USD';
  public compareProducts: BehaviorSubject<Product[]> = new BehaviorSubject([]);
  public compareProduct: Product[] = [];
  private datos: Login;

  constructor(private http: HttpClient, private toastrService: ToastrService) {
    this.compareProducts.subscribe(products => this.compareProduct = products);
    this.datos = {
      frixionista: false
    };
  }

  // Observable Product Array
  private products(): Observable<Product[]> {
    if (localStorage.getItem(btoa('auth')) !== null) {
      const data = JSON.parse(atob(localStorage.getItem(btoa('auth'))));
      if (data.frixionista) {
        this.currency = 'MAY';
      } else {
        this.currency = 'COP';
      }
    } else {
      this.currency = 'COP';
    }
    return this.http.get<Product[]>(`${environment.url}products`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {money: this.currency}
    }).pipe(map((response: HttpResponse<Product[]>) => {
      if (this.currency !== 'USD') {
        this.currency = 'USD';
      }
      if (localStorage.getItem(btoa('auth')) !== null) {
        this.datos = JSON.parse(atob(localStorage.getItem(btoa('auth'))));
      }
      response.body.forEach(e => {
        if (!this.datos.frixionista) {
          e.price = this.redondeaAlAlza(parseFloat(e.price.toString()) + (e.price * 0.04));
          e.salePrice = this.redondeaAlAlza(e.salePrice + (e.salePrice * 0.04));
        }
        e.ref = e.shortDetails.replace(' ', '-');
        e.ref = e.ref.replace('/', '-');
      });
      return response.body;
    }));
  }

  private redondeaAlAlza(x, r = 1000) {
    let xx = Math.floor(x / r);
    if (xx !== x / r) {
      xx++;
    }
    return (xx * r);
  }

  // Get Products
  public getProducts(): Observable<Product[]> {
    return this.products();
  }

  // Get Products By Ref
  public getProduct(ref: string): Observable<Product> {
    return this.products().pipe(map(items => {
      return items.find((item: Product) => {
        return item.ref === ref;
      });
    }));
  }

  // Get Products By category
  public getProductByCategory(category: string): Observable<Product[]> {
    return this.products().pipe(map(items =>
      items.filter((item: Product) => {
        if (category === 'all') {
          return item;
        } else {
          if (item.category == category || item.sub_categories == category || item.collection == category) {
            return item;
          }
        }

      })
    ));
  }

  // Get Products
  public getComapreProducts(): Observable<Product[]> {
    return this.compareProducts.asObservable();
  }

  // If item is aleready added In compare
  public hasProduct(product: Product): boolean {
    const item = this.compareProduct.find(item => item.id === product.id);
    return item !== undefined;
  }

  // Add to compare
  public addToCompare(product: Product): Product | boolean {
    let item: Product | boolean = false;
    if (this.hasProduct(product)) {
      item = this.compareProduct.filter(item => item.id === product.id)[0];
      const index = this.compareProduct.indexOf(item);
    } else {
      if (this.compareProduct.length < 4) {
        this.compareProduct.push(product);
      } else {
        this.toastrService.warning('Máximo 4 productos en comparación.'); // toasr services
      }
    }
    return item;
  }

  // Removed Product
  public removeFromCompare(product: Product) {
    if (product === undefined) {
      return;
    }
    const index = this.compareProduct.indexOf(product);
    this.compareProduct.splice(index, 1);
  }

}
