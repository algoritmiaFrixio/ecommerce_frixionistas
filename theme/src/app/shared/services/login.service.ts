import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Login} from '../classes/login/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public show: boolean;
  public data: Login;
  constructor(private htpp: HttpClient) {
    this.show = localStorage.getItem(btoa('auth')) !== null;
    this.data=localStorage.getItem(btoa('auth')) !== null?JSON.parse(atob(localStorage.getItem(btoa('auth')))):{};
  }
  public setData(){
    this.data=localStorage.getItem(btoa('auth')) !== null?JSON.parse(atob(localStorage.getItem(btoa('auth')))):{};
    this.show = localStorage.getItem(btoa('auth')) !== null;
  }
  public login(data): Observable<Login> {
    return this.htpp.post<Login>(`${environment.url}authFrixionista`, data, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Login>) => response.body));
  }

  public updateAccount(data, id): Observable<Login> {
    return this.htpp.put<Login>(`${environment.url}client/${id}`, data, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Login>) => response.body));
  }

  public uploadFile(fileToUpload: File) {
    const _formData = new FormData();
    _formData.append('excel', fileToUpload, fileToUpload.name);
    return this.htpp.post(`${environment.url}excel`, _formData);
  }
}
