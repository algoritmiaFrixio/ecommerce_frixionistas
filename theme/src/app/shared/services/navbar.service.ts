import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Categories} from '../classes/categories';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  constructor(private _http: HttpClient) {
  }

  public getCategories(): Observable<Categories[]> {
    return this._http.get<Categories[]>(`${environment.url}categories`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Categories[]>) => response.body));
  }
}
