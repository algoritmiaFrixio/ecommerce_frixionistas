import {Injectable} from '@angular/core';
import {Order} from '../classes/order';
import {Router} from '@angular/router';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class OrderService {

  // Array
  public OrderDetails;

  constructor(private router: Router, private http: HttpClient) {
    this.OrderDetails = localStorage.getItem(btoa('item')) !== null ? JSON.parse(atob(localStorage.getItem(btoa('item')))) : null;
  }

  // Get order items
  public getOrderItems(): Order {
    this.OrderDetails = localStorage.getItem(btoa('item')) !== null ? JSON.parse(atob(localStorage.getItem(btoa('item')))) : {};
    return this.OrderDetails;
  }

  public confirmartion(data) {
    return this.http.post(`${environment.url}confirmation`, data, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map(response => response));
  }

  public getCountries() {
    return this.http.get('https://restcountries.eu/rest/v2/all').pipe(map((response) => response));
  }

  public payu(data): Observable<any> {
    return this.http.post<any>(`${environment.url}pago`, data, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public cash(data, name, id, email) {
    window.open(`${environment.url}efectivo?data=${data}&id=${id}&name=${name}&email=${email}`);
  }

  public trans(data, name, id, email) {
    return this.http.post<any>(`${environment.url}transacion`, {data, id, name, email}, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  // Create order
  public createOrder(product: any, details: any, orderId: any, amount: any, shipping: number, tax: number, credit: boolean, descuento: number) {
    const item = {
      shippingDetails: details,
      product: product,
      orderId: orderId,
      totalAmount: amount,
      shipping,
      tax,
      descuento
    };
    this.OrderDetails = item;
    localStorage.setItem(btoa('item'), btoa(JSON.stringify(item)));
    if (credit) {
      location.href = 'https://api.frixiohechoamano.com/api-eCommerce/public/index.php?id=' + orderId;
    }
  }
}
