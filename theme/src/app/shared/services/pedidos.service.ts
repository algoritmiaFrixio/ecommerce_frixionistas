import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pedidos} from '../../staff/interfaces/pedidos/pedidos';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Products} from '../../staff/interfaces/pedidos/products';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  constructor(private http: HttpClient) {
  }

  public Orders(curr): Observable<Pedidos[]> {
    return this.http.get<Pedidos[]>(`${environment.url}order?id=${curr}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Pedidos[]>) => {
      response.body.forEach(e => {
        e.cantidad = 0;
        e.products.forEach(ele => {
          e.cantidad += parseInt(String(ele.quantity));
        });
      });
      return response.body;
    }));
  }

  public getOrders(curr): Observable<Pedidos[]> {
    return this.Orders(curr);
  }

  public getTotalAmount(mult: Products[]): number {
    let total = 0;
    mult.forEach(e => {
      total += e.price * e.quantity;
    });
    return total;
  }

  public getGreatTotal(item: Pedidos[]): number {
    let total = 0;
    item.forEach(e => {
      e.products.forEach(ele => {
        total += ele.price * ele.quantity;
      });
    });
    return total;
  }
}
