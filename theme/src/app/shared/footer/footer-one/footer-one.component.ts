import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-footer-one',
  templateUrl: './footer-one.component.html',
  styleUrls: ['./footer-one.component.scss']
})
export class FooterOneComponent implements OnInit {

  constructor(public _Location: Location, public CartService: CartService) { }

  ngOnInit() {
  }

}
