// Product Colors
export type ProductColor =
  'white'
  | 'black'
  | 'red'
  | 'green'
  | 'purple'
  | 'yellow'
  | 'blue'
  | 'gray'
  | 'orange'
  | 'pink';

// Product Size
// export type ProductSize = 'M' | 'L' | 'XL';

// Product Tag
export type ProductTags = 'nike' | 'puma' | 'lifestyle' | 'caprese';

// Product
export interface Product {
  id?: number;
  name?: string;
  price?: number;
  priced?: number;
  salePrice?: number;
  discount?: number;
  pictures?: string;
  shortDetails?: string;
  description?: string;
  stock?: number;
  new?: boolean;
  sale?: boolean;
  category?: string;
  sub_categories?: string;
  tela?: string;
  tecnica?: string;
  color?: string;
  colorSpanish?: string;
  references?: string;
  sizes?: string;
  colors?: ProductColor[];
  size?: string[];
  tags?: string[];
  ref?: string;
  collection?: string;
  variants?: any[];
}

// Color Filter
export interface ColorFilter {
  color?: ProductColor;
}

// Tag Filter
export interface TagFilter {
  tag?: ProductTags;
}
