export interface Categories {
  id?: number;
  name?: string;
  sub_categories?: SubCategories[];
}

export interface SubCategories {
  id?: number;
  name?: string;
}
