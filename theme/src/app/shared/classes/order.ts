import {CartItem} from './cart-item';

// Order
export interface Order {
  shippingDetails?: any;
  shipping?: number;
  descuento?: number;
  tax?: number;
  product?: CartItem[];
  orderId?: any;
  totalAmount?: any;
}
