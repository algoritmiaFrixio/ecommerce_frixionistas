--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-02-25 16:44:41

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 211 (class 1259 OID 18907)
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    discount double precision DEFAULT '0'::double precision NOT NULL,
    "shortDetails" character varying(25) NOT NULL,
    "colorSpanish" character varying(200),
    "references" character varying(255),
    tela character varying(100) NOT NULL,
    tecnica character varying(100) NOT NULL,
    description text NOT NULL,
    stock integer NOT NULL,
    new boolean DEFAULT true NOT NULL,
    sale boolean DEFAULT true NOT NULL,
    id_subcategory integer,
    id_collection integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.products OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 18905)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- TOC entry 2896 (class 0 OID 0)
-- Dependencies: 210
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- TOC entry 2761 (class 2604 OID 19052)
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- TOC entry 2890 (class 0 OID 18907)
-- Dependencies: 211
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, name, discount, "shortDetails", "colorSpanish", "references", tela, tecnica, description, stock, new, sale, id_subcategory, id_collection, created_at, updated_at) FROM stdin;
3	Blusa manga campana bordado en patecabra, pasado y cadeneta	0	Ref 01/778	Blanco	\N	Lino Taylor 100% Rayon	PATECABRA, PASADO, CADENETA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 15:52:14	2019-01-05 15:52:14
4	Blusa camisera bordada al pasado y patecabra	0	Ref 01/779	Coral	\N	Lino Margarita 55% Ramie 45% Algodón	PASADO, PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 15:57:49	2019-01-05 15:57:49
11	Pantalón resorte atrás	0	Ref 01/785P	Blanco	\N	Lino Zeus 100% Lino	N/A	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	5	1	2019-01-05 16:45:10	2019-01-05 16:45:10
5	Blusa campesina manga bolero en sesgos y patecabra	0	Ref 01/780	Verde petroleo	\N	Lino Margarita 55% Ramie 45% Algodón	SESGOS, PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 16:04:31	2019-01-05 16:04:31
6	Blusón cuello neru bordado al pasado	0	Ref 01/781	Blanco	\N	Taylor 100% Rayon	PASADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 16:16:04	2019-01-05 16:16:04
7	Blusa manga japonesa en patecabra y sesgos	0	Ref 01/782	Blanco	\N	Lino Margarita 55% Ramie 45% Algodón	PATECABRA, SESGOS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 16:22:37	2019-01-05 16:22:37
8	Blusa manga campana bordada al pasado	0	Ref 01/783	Verde pistacho	\N	Chalis 100% Rayon	PASADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 16:28:48	2019-01-05 16:28:48
9	Blusón cuello neru manga 3/4 bordada al pasado	0	Ref 01/784	Blanco	\N	Taylor 100% Rayon	PASADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 16:34:27	2019-01-05 16:34:27
12	Kaflan bordado en pespunte y pasado	0	Ref 01/786	Azul	\N	Chambray 100% Algodón	PESPUNTE, PASADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	3	1	2019-01-05 16:49:04	2019-01-05 16:49:04
15	Blusa manga corta frente y ruedo bordado en patecabra y arañas	0	Ref 01/788	Rojo	\N	Lino Margarita 55% Ramie 45% Algodón	PATECABRA, ARAÑAS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 17:21:03	2019-01-05 17:21:03
16	Kaflan cartera corta bordado en patecabra	0	Ref 01/789	Blanco	\N	Lino Zeus 100% Lino	PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	3	1	2019-01-08 13:54:52	2019-01-08 13:54:52
17	Vestido bordado al pasado en colores	0	Ref 05/288	Negro	\N	Canali 100% Rayon	PASADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	2	1	2019-01-08 13:59:13	2019-01-08 13:59:13
18	Blusón o vestido bordado en patecabra	0	Ref 01/790	Amarillo	\N	Chalis 100% Rayon	PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	2	1	2019-01-08 19:39:19	2019-01-08 19:39:19
19	Blusa cordon tubular bordado en patecabra	0	Ref 01/791	Blanco	\N	Taylor 100% Rayon	PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-08 19:48:31	2019-01-08 19:48:31
22	Blusa sisa bordada en arañas y pasado	0	Ref 01/793	Azul claro	\N	Lino Zeus 100% Lino	ARAÑAS, PASADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-08 20:06:37	2019-01-08 20:06:37
23	Blusa manga larga bordada en araña y patecabra tejida	0	Ref 01/794	Azul rey	\N	Margarita 55% Ramie 45% Algodón	ARAÑA, PATECABRA TEJIDA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-08 20:10:54	2019-01-08 20:10:54
24	Vestido talego con cinturon ruedo manga en patecabra	0	Ref 05/289	Azul navy	\N	Lino Paola 55% Lino 45% Algodón	PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	2	1	2019-01-08 20:22:05	2019-01-08 20:22:05
25	Camisa manga larga un bolsillo calada	0	Ref 08/464	Beige	\N	Lino Zeus 100% Lino	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-08 20:40:12	2019-01-08 20:40:12
27	Guayabera manga larga randa y calado	0	Ref 08/466	Azul claro	\N	Lino Zeus 100% Lino	CALADO, RANDA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-08 20:56:25	2019-01-08 20:56:25
28	Camisa manga corta cuello neru bordado en 20u y randas	0	Ref 08/465	Verde menta	\N	Lino Margarita 55% Ramie 45% Algodón	20U, RANDAS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	11	1	2019-01-08 20:59:06	2019-01-08 20:59:06
29	Camisa manga larga con sesgos	0	Ref 08/467	Azul navy	\N	Lino Paola 55% Lino 45% Algodón	SESGOS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-08 21:03:57	2019-01-08 21:03:57
30	Guayabera manga larga calada y randa	0	Ref 08/468	Gris	\N	Lino Margarita 55% Ramie 45% Algodón	CALADO, RANDA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-08 21:09:07	2019-01-08 21:09:07
31	Camisa Manga Larga con calado	0	Ref 08/469	Rosado	\N	Lino Paola 55% Lino 45% Algodón	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-08 21:11:45	2019-01-08 21:11:45
32	Camisa manga corta con alforzas y calado	0	Ref 08/470	Azul navy	\N	Lino Paola 55% Lino 45% Algodón	ALFORZAS, CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	11	1	2019-01-08 21:17:47	2019-01-08 21:17:47
33	Guayabera manga larga calada	0	Ref 08/471	Amarillo	\N	Lino Paola 55% Lino 45% Algodón	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-08 21:22:05	2019-01-08 21:22:05
34	Guayabera manga larga calada con alforzas	0	Ref 08/472	Blanco	\N	Lino Zeus 100% Lino	CALADO, ALFORZAS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-09 14:11:53	2019-01-09 14:11:53
35	Guayabera con alforzas, sesgo, calado y randas	0	Ref 08/473	Blanco	\N	Lino Zeus 100% Lino	CALADO, ALFORZAS, SESGO, RANDAS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-09 14:16:22	2019-01-09 14:16:22
10	Blusa manga malla bordada en rococo brasilero	0	Ref 01/785	Blanco	Pantalon se vende por separado,Ref 01/785P	Lino Zeus 100% Lino	ROCOCO BRASILERO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-05 16:38:54	2019-01-05 16:38:54
13	Kaflan bordado en patecabra y arañas	0	Ref 01/787	Palo de rosa	Pantalon se vende por separado,Ref 01/787P	Canali 100% Rayon	PATECABRA, ARAÑAS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	3	1	2019-01-05 16:57:55	2019-01-05 16:57:55
45	Chaleco de niño	0	Ref 08/481CH	Beige	Guayabera de niño se vende por separado,Ref 08/481N	Lino Zeus 100% Lino	N/A	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	16	1	2019-01-09 15:49:17	2019-01-09 15:49:17
14	Pantalón forrado	0	Ref 01/787P	Palo de rosa	Kaflan se vende  por separado,Ref 01/787	Canali 100% Rayon	N/A	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	5	1	2019-01-05 17:16:47	2019-01-05 17:16:47
43	Guayabera manga larga calada	0	Ref 08/481	Blanco	Guayabera niño se vende por separado,Ref 08/481N;Chaleco niño se vende por separado,Ref 08/481CH	Lino Margarita 55% Ramie 45% Algodón	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-09 15:37:43	2019-01-09 15:37:43
44	Guayabera de niño manga larga calada	0	Ref 08/481N	Blanco	Chaleco para niño se vende por separado,Ref 08/481CH	Lino Margarita 55% Ramie 45% Algodón	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	16	1	2019-01-09 15:41:35	2019-01-09 15:41:35
20	Blusa manga japonesa cuello en V bordado en patecabra	0	Ref 01/792B	Fucsia	Falda se vende por separado,Ref 01/792F	Lino 100% lino	PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	1	1	2019-01-08 19:54:22	2019-01-08 19:54:22
21	Falda semi ajustada con bolero	0	Ref 01/792F	Fucsia	Blusa se vende por separado,Ref 01/792B	Lino 100% lino	PATECABRA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	4	1	2019-01-08 19:59:07	2019-01-08 19:59:07
36	Camisa Slim Fit manga larga calada	0	Ref 08/474	Palo de rosa	\N	Lino Daiquiri100% Lino	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	9	1	2019-01-09 14:24:42	2019-01-09 14:24:42
37	Camisa Slim Fit manga larga calada	0	Ref 08/475	Azul marino	\N	Lino Paola 55% Lino 45% Algodón	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	9	1	2019-01-09 14:39:14	2019-01-09 14:39:14
38	Camisa manga corta con sesgos en chambray calada	0	Ref 08/476	Blanco	\N	Lino Margarita 55% Ramie 45% Algodón	CALADO, SESGOS	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	11	1	2019-01-09 14:50:27	2019-01-09 14:50:27
39	Camisa manga corta calada y randa	0	Ref 08/477	Índigo	\N	Chambray 100% Algodón	CALADO, RANDA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	11	1	2019-01-09 14:54:17	2019-01-09 14:54:17
40	Camisa cuello neru doble con metido en chambray y calado	0	Ref 08/478	Blanco	\N	Chambray 100% Algodón	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-09 15:11:58	2019-01-09 15:11:58
41	Camisa manga corta cuello italiano calada	0	Ref 08/479	Coral	\N	Lino Margarita 55% Ramie 45% Algodón	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	11	1	2019-01-09 15:14:42	2019-01-09 15:14:42
42	Guayabera manga larga calada y randa	0	Ref 08/480	Verde esmeralda	\N	Lino Margarita 55% Ramie 45% Algodón	CALADO, RANDA	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-09 15:24:39	2019-01-09 15:24:39
46	Guayabera manga larga calada	0	Ref 08/482	Beige	\N	Lino Zeus 100% Lino	CALADO	El tiempo de despacho es de 10 días laborales enviándose desde la ciudad de  Cartago Valle del Cauca.\n*No aplica para prendas elaboradas en sesgos. 	5	t	f	10	1	2019-01-09 15:51:53	2019-01-09 15:51:53
47	Pago de afiliación	0	Pago de/Afiliación	N/A\n	\N	N/A	N/A	Haz clic en añadir al carrito y luego haz clic en "Pagar" en el icono de tu carrito de compras. Al completar la afiliación por este medio solo tienes que esperar tus catálogos en tu dirección de despacho, se incluyen todos los catálogos vigentes hasta la fecha, talonario de pedidos y lista precios	1	t	t	\N	\N	2019-01-09 15:51:53	2019-01-09 15:51:53
\.


--
-- TOC entry 2897 (class 0 OID 0)
-- Dependencies: 210
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 47, true);


--
-- TOC entry 2763 (class 2606 OID 18918)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- TOC entry 2765 (class 2606 OID 18930)
-- Name: products products_shortdetails_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_shortdetails_unique UNIQUE ("shortDetails");


--
-- TOC entry 2767 (class 2606 OID 18924)
-- Name: products products_id_collection_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_id_collection_foreign FOREIGN KEY (id_collection) REFERENCES public.collections(id);


--
-- TOC entry 2766 (class 2606 OID 18919)
-- Name: products products_id_subcategory_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_id_subcategory_foreign FOREIGN KEY (id_subcategory) REFERENCES public.sub_categories(id);


-- Completed on 2019-02-25 16:44:42

--
-- PostgreSQL database dump complete
--

