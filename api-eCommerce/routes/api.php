<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('categories', 'CategoryController');
Route::apiResource('sizes', 'SizeController');
Route::apiResource('products', 'ProductController');
Route::get('product', 'ProductController@all');
Route::post('moreProducts', 'ProductController@excel');
Route::apiResource('colors', 'ColorController');
Route::post('auth', 'UserController@auth');
Route::post('payu', 'PagoPayuController@payu');
Route::post('confirmation', 'PagoPayuController@confirmation');
Route::get('efectivo', 'PagoPayuController@cash');
Route::post('transacion', 'PagoPayuController@transacion');
Route::apiResource('pago', 'PagoPayuController');
Route::apiResource('orders', 'OrderController');
Route::get('order', 'OrderController@order');
Route::apiResource('collections', 'CollectionController');
//Route::post('excel', 'ClientController@excel');
Route::post('authFrixionista', 'ClientController@authFrixionista');
Route::put('changeStatus', 'OrderController@changeStatus');
Route::apiResource('client', 'ClientController');
Route::get('prueba', 'OrderController@prueba');
