<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sub_category extends Model
{
    protected $hidden = ['updated_at', 'created_at'];

    protected function category()
    {
        return $this->belongsTo(category::class, 'id_category', 'id');
    }
}
