<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pago_payu extends Model
{
    protected $fillable = ['description', 'referenceCode', 'amount', 'taxReturnBase', 'currency', 'buyerEmail', 'buyerFullName', 'shippingAddress', 'shippingCity', 'shippingCountry', 'postalcode', 'shippingValue',
        'state', 'telephone', 'tax','dtco'];
    protected $hidden = ['created_at', 'updated_at'];
}
