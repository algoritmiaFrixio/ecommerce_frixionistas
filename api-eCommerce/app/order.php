<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $fillable = ['products', 'quantity', 'size','cedula'];
    protected $hidden = ['updated_at'];
}
