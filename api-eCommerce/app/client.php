<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    protected $fillable = ['buyerEmail', 'buyerFullName', 'telephone', 'shippingCity', 'state', 'shippingCountry', 'postalcode', 'shippingAddress', 'identification'];
    protected $hidden = ['created_at', 'updated_at'];
}
