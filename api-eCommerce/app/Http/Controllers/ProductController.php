<?php

namespace App\Http\Controllers;

use App\pictures;
use App\pivot_price;
use App\pivot_size;
use App\product;
use App\size;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class ProductController extends
    Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isJson()) {
            $product = DB::table('products')
                ->join('sub_categories', 'sub_categories.id', '=', 'products.id_subcategory')
                ->join('categories', 'categories.id', '=', 'sub_categories.id_category')
                ->join('collections', 'collections.id', '=', 'products.id_collection')
                ->where('products.new', false)
                ->select('products.id', 'products.name', 'products.discount', 'products.shortDetails', 'products.description', 'products.stock', 'products.colorSpanish', 'products.new', 'products.references', 'products.sale', 'products.tela', 'products.tecnica', 'categories.name as category', 'sub_categories.name as sub_categories', 'collections.name as collection')
                ->get();
            $tmp = DB::table('products')
                ->whereNull('id_subcategory')
                ->select('products.id', 'products.name', 'products.discount', 'products.shortDetails', 'products.description', 'products.stock', 'products.colorSpanish', 'products.new', 'products.references', 'products.sale', 'products.tela', 'products.tecnica')
                ->get();
            foreach ($tmp as $prod) {
                $product[] = $prod;
            }
            foreach ($product as $index => $item) {
                $variants = DB::table('pictures')
                    ->join('colors', 'colors.id', '=', 'pictures.id_color')
                    ->select('pictures.path as images', 'colors.color')
                    ->where('pictures.id_product', $item->id)
                    ->get();
                $pictures = DB::table('pictures')
                    ->select('pictures.path as images')
                    ->where('pictures.id_product', $item->id)
                    ->whereNull('pictures.id_color')
                    ->get();
                $sizes = DB::table('pivot_sizes')
                    ->join('sizes', 'pivot_sizes.id_size', '=', 'sizes.id')
                    ->where('pivot_sizes.id_product', $item->id)
                    ->select('sizes.name')
                    ->get();
                $price = DB::table('pivot_prices')
                    ->join('prices', 'prices.id', '=', 'pivot_prices.id_price')
                    ->where('pivot_prices.id_product', $item->id)
                    ->where('prices.type_money', $request->money)
                    ->select('pivot_prices.price')
                    ->get();
                $product[$index]->variants = $variants;
                $product[$index]->pictures = [];
                $product[$index]->colors = [];
                $product[$index]->sizes = [];
                $product[$index]->price = $price[0]->price;
                $tmp = ($item->discount / 100) + 1;
                $color = '';
                $size = '';
                $product[$index]->salePrice = $price[0]->price * $tmp;
                foreach ($variants as $index5 => $item2) {
                    $color = count($variants) - 1 > $index5 ? $color . $item2->color . ', ' : $color . $item2->color;
                    $product[$index]->colors[] = $item2->color;
                    $product[$index]->pictures[] = $item2->images;
                }
                foreach ($sizes as $index6 => $item3) {
                    $size = count($sizes) - 1 > $index6 ? $size . $item3->name . ', ' : $size . $item3->name;
                    $product[$index]->size[] = $item3->name;
                }
                foreach ($pictures as $item4) {
                    $product[$index]->pictures[] = $item4->images;
                }
                $product[$index]->color = $color;
                $product[$index]->sizes = $size;
            }
            return response()->json($product, 200);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function all()
    {
        $products = product::all();
        foreach ($products as $item) {
            $item->sub_category->category;
            foreach ($item->sizes as $size) {
                $size->sizes;
            }
            $item->prices;

        }
        return response($products, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        try {
            $product = new product();
            $product->fill($request->all());
            $product->saveOrFail();
            $arrSizes = explode(',', $request->sizes);
            foreach ($arrSizes as $item) {
                $pivotSizes = new pivot_size();
                $pivotSizes->id_size = $item;
                $pivotSizes->id_product = $product->id;
                $pivotSizes->saveOrFail();
            }
            $arrPrice = explode(',', $request->price);
            foreach ($arrPrice as $index => $item) {
                $pivotPrices = new pivot_price();
                $pivotPrices->id_price = $index + 1;
                $pivotPrices->id_product = $product->id;
                $pivotPrices->price = $item;
                $pivotPrices->saveOrFail();
            }
            $arrVariants = explode(',', $request->variants);
            $arrNames = [];
            for ($i = 0; $i < count($request->allFiles()); $i++) {
                $tmp = time() . $request->file('files' . $i)->getClientOriginalName();
                $request->file('files' . $i)->move('../images/product/' . $product->id, $tmp);
                $arrNames[] = 'https://api.frixiohechoamano.com/api-eCommerce/images/product/' . $product->id . '/' . $tmp;
            }
            foreach ($arrNames as $index => $item) {
                $pictures = new pictures();
                $pictures->path = $item;
                $pictures->id_product = $product->id;
                if ($arrVariants[$index] !== "") {
                    $pictures->id_color = $arrVariants[$index];
                }
                $pictures->saveOrFail();
            }
            return response()->json($product, 201);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function excel(Request $request)
    {
        try {
            if ($request->hasFile('excel')) {
                Excel::load($request->file('excel'), function ($reader) {
                    $result = $reader->get();
                    foreach ($result as $item) {
                        $product = new product();
                        $product->shortDetails = 'Ref ' . $item->ref;
                        $product->name = $item->name;
                        $product->tela = $item->tela;
                        $product->colorSpanish = $item->color;
                        $product->description = 'Despacho inmediato.* Verificar primero existencia al WhatsApp.';
                        $product->id_collection = $item->collection;
                        $product->stock = rand(10, 50);
                        $product->new = false;
                        $product->sale = true;
                        $product->id_subcategory = $item->subcategory;
                        $product->tecnica = $item->tecnica;
                        $product->saveOrFail();
                        $arrSizes = explode(',', $item->sizes);
                        foreach ($arrSizes as $item2) {
                            $size = size::where('name', $item2)->firstOrFail();
                            $pivotSizes = new pivot_size();
                            $pivotSizes->id_size = $size->id;
                            $pivotSizes->id_product = $product->id;
                            $pivotSizes->saveOrFail();
                        }
                        $arrPrice = [0, 0, $item->price, $item->price];
                        foreach ($arrPrice as $index => $item3) {
                            $pivotPrices = new pivot_price();
                            $pivotPrices->id_price = $index + 1;
                            $pivotPrices->id_product = $product->id;
                            $pivotPrices->price = $item3;
                            $pivotPrices->saveOrFail();
                        }
                        $pictures = new pictures();
                        $ref = str_replace('/', '-', $item->ref);
                        $pictures->path = 'https://api.frixiohechoamano.com/api-eCommerce/images/product/' . $ref . '.jpg';
                        $pictures->id_product = $product->id;
                        $pictures->id_color = $item->colors;
                        $pictures->saveOrFail();
                    }
                });
            }
            return response()->json(['message' => 'ok'], 201);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product $product
     *
     * @return \Illuminate\Http\Response
     */
    public
    function show(product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product $product
     *
     * @return \Illuminate\Http\Response
     */
    public
    function edit(product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\product $product
     *
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, product $product)
    {
        try {
            $product->fill($request->all());
            $product->saveOrFail();
            $product->sizes;
            $tmp = $product->sizes;
            $tmp2 = $request->sizes;
            if (count($product->sizes) !== count($request->sizes)) {
                $add = count($product->sizes) < count($request->sizes);
                foreach ($product->sizes as $index => $old) {
                    foreach ($request->sizes as $index2 => $new) {
                        if ($old->id_size === $new) {
                            unset($tmp[$index]);
                            unset($tmp2[$index2]);
                        }
                    }
                }
                if ($add) {
                    foreach ($tmp2 as $item) {
                        $pivotSizes = new pivot_size();
                        $pivotSizes->id_size = $item;
                        $pivotSizes->id_product = $product->id;
                        $pivotSizes->saveOrFail();

                    }
                } else {
                    foreach ($tmp as $item) {
                        $del = pivot_size::where('id', $item->id)->firstOrFail();
                        $del->delete();
                    }
                }
            }
            $arrPrice = explode(',', $request->price);
            foreach ($arrPrice as $index => $item) {
                $pivotPrices = pivot_price::where('id_price', $index + 1)->firstOrFail();
                $pivotPrices->price = $item;
                $pivotPrices->saveOrFail();
            }
            $products = product::all();
            foreach ($products as $item) {
                $item->sub_category->category;
                foreach ($item->sizes as $size) {
                    $size->sizes;
                }
                $item->prices;

            }
            return response()->json(['message' => 'Actualizado correctamente', 'products' => $products], 201);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    /**
     * @param product $product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public
    function destroy(product $product, Request $request)
    {
        if ($request->isJson()) {
            $sizes = pivot_size::where('id_product', $product->id)->get();
            $prices = pivot_price::where('id_product', $product->id)->get();
            $pictures = pictures::where('id_product', $product->id)->get();
            foreach ($sizes as $item) {
                $item->delete();
            }
            foreach ($prices as $item) {
                $item->delete();
            }
            foreach ($pictures as $item) {
                $item->delete();
            }
            if ($product->delete()) {
                $products = product::all();
                foreach ($products as $item) {
                    foreach ($item->sizes as $size) {
                        $size->sizes;
                    }
                    $item->prices;

                }
                return response()->json(['message' => 'Eliminado correctamente', 'products' => $products], 200);
            }
            return response()->json(['message' => 'ocurrio un error'], 500);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }
}
