<?php

namespace App\Http\Controllers;

use App\client;
use App\order;
use App\pago_payu;
use App\product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = order::all();
        $products = [];
        foreach ($orders as $index => $item) {
            $products[$index] = [];
            $pago = pago_payu::where('id', $item->id_pago)->firstOrFail();
            $currency = 'COP';
            $orders[$index]->frixionista = false;
            $porc = 1;
            if (!is_null($item->id_client)) {
                $client = client::where('id', $item->id_client)->firstOrFail();
                $currency = $client->frixionista ? 'MAY' : 'COP';
                $orders[$index]->frixionista = $client->frixionista;
//                $porc = $client->frixionista ? 1 : 1.04;
            }
            $orders[$index]->pago = $pago;
            if (strlen($item->products) > 1) {
                $arr = explode(',', $item->products);
                $arrzize = explode(',', $item->size);
                $arrquantity = explode(',', $item->quantity);
                unset($orders[$index]->size);
                unset($orders[$index]->quantity);
                $orders[$index]->products = [];
                foreach ($arr as $index2 => $item2) {
                    $tmp = product::where('id', $item2)->firstOrFail();
                    $tmp->size = $arrzize[$index2];
                    $tmp->quantity = $arrquantity[$index2];
                    $tmp->pictures;
                    $price = DB::table('pivot_prices')
                        ->join('prices', 'prices.id', '=', 'pivot_prices.id_price')
                        ->where('pivot_prices.id_product', $tmp->id)
                        ->where('prices.type_money', $currency)
                        ->select('pivot_prices.price')
                        ->get();
                    $tmp->price = ceil(($price[0]->price * $porc) / 1000) * 1000;
                    $products[$index][] = $tmp;
                }
            } else {
                $tmp = product::where('id', $item->products)->firstOrFail();
                $tmp->size = $item->size;
                $tmp->quantity = $item->quantity;
                unset($orders[$index]->size);
                unset($orders[$index]->quantity);
                $tmp->pictures;
                $price = DB::table('pivot_prices')
                    ->join('prices', 'prices.id', '=', 'pivot_prices.id_price')
                    ->where('pivot_prices.id_product', $tmp->id)
                    ->where('prices.type_money', $currency)
                    ->select('pivot_prices.price')
                    ->get();
//                $tmp->price = ceil(($price[0]->price * $porc) / 1000) * 1000;
                $products[$index][] = $tmp;
            }
        }
        foreach ($orders as $index => $item) {
            $orders[$index]->products = $products[$index];
        }
        return response()->json($orders, 200);
    }

    public function order(Request $request)
    {
        $orders = order::all()->where('id_cliente', $request->id);
        $products = [];
        foreach ($orders as $index => $item) {
            $products[$index] = [];
            $pago = pago_payu::where('id', $item->id_pago)->firstOrFail();
            $currency = 'COP';
            if (!is_null($item->id_client)) {
                $client = client::where('id', $item->id_pago)->firstOrFail();
                $currency = $client->frixionista ? 'MAY' : 'COP';
            }
            $orders[$index]->pago = $pago;
            if (strlen($item->products) > 1) {
                $arr = explode(',', $item->products);
                $arrzize = explode(',', $item->size);
                $arrquantity = explode(',', $item->quantity);
                unset($orders[$index]->size);
                unset($orders[$index]->quantity);
                $orders[$index]->products = [];
                foreach ($arr as $index2 => $item2) {
                    $tmp = product::where('id', $item2)->firstOrFail();
                    $tmp->size = $arrzize[$index2];
                    $tmp->quantity = $arrquantity[$index2];
                    $tmp->pictures;
                    $price = DB::table('pivot_prices')
                        ->join('prices', 'prices.id', '=', 'pivot_prices.id_price')
                        ->where('pivot_prices.id_product', $tmp->id)
                        ->where('prices.type_money', $currency)
                        ->select('pivot_prices.price')
                        ->get();
                    $tmp->price = $price[0]->price;
                    $products[$index][] = $tmp;
                }
            } else {
                $tmp = product::where('id', $item->products)->firstOrFail();
                $tmp->size = $item->size;
                $tmp->quantity = $item->quantity;
                unset($orders[$index]->size);
                unset($orders[$index]->quantity);
                $tmp->pictures;
                $price = DB::table('pivot_prices')
                    ->join('prices', 'prices.id', '=', 'pivot_prices.id_price')
                    ->where('pivot_prices.id_product', $tmp->id)
                    ->where('prices.type_money', $currency)
                    ->select('pivot_prices.price')
                    ->get();
                $tmp->price = $price[0]->price;
                $products[$index][] = $tmp;
            }
        }
        foreach ($orders as $index => $item) {
            $orders[$index]->products = $products[$index];
        }
        $return = [];
        foreach ($orders as $item) {
            $return[] = $item;
        }
        return response()->json($return, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function show(order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        //
    }

    public function changeStatus(Request $request)
    {
        if ($request->isJson()) {
            try {
                $order = order::where('id', $request->id)->firstOrFail();
                $order->status = $request->status;
                $order->saveOrFail();
                return response()->json(true, 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function prueba(Request $request)
    {

//        try{
//            $request->validate([
//                'name' => 'required|string|max:30',
//                'identification' => 'required|string|max:20',
//                'address' => 'required|string|max:70',
//                'telephone' => 'required',
//                'referred' => 'required|string|max:40',
//                'id_legal_group' => 'required|integer',
//                'counterpart' => 'required|string|max:40',
//                'observations' => 'required|string|max:500',
//                'rol_client' => 'required|string|max:25'
//            ], [
//                'required' => 'El campo Es requerido',
//                'string' => 'El campo debería tener un formato valido',
//                'max' => 'El campo debería tener menor caracteres',
//                'integer' => 'El campo debería tener un formato valido'
//            ]);
//        }catch (ModelNotFoundException $e){
            return response()->json(['hola'], 200);
//        }
    }
}
