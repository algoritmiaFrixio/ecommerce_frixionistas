<?php

namespace App\Http\Controllers;

use App\user;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\user $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        //
    }

    public function auth(Request $request)
    {
        if ($request->isJson()) {
            try {
                $auth = user::where('usuario', $request->usuario)->firstOrFail();
                if (Hash::check($request->contrasena, $auth->contrasena)) {
                    return response()->json(['token' => $auth->token], 200);
                }
                return response()->json(['message' => 'Usuario y/o contraseña incorrectos'], 401);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => 'Usuario y/o contraseña incorrectos'], 401);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }
}
