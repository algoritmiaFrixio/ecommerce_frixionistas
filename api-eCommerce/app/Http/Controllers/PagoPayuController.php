<?php

namespace App\Http\Controllers;

use App\client;
use App\order;
use App\pago_payu;
use App\product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Barryvdh\DomPDF\Facade as PDF;

class PagoPayuController extends
    Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isJson()) {
            try {
                $pago = new pago_payu();
                $pago->fill($request->all());
                $order = new order();
                $order->fill($request->all());
                $order->id_cliente = $request->has('id_cliente') ? $request->id_cliente : null;
                if (!is_null($request->password)) {
                    $client = new client();
                    $client->fill($request->all());
                    $client->password = Hash::make($request->password);
                    $client->saveOrFail();
                    $order->id_cliente = $client->id;
                } else if (!is_null($request->id_client)) {
                    $client = client::where('id', $request->id_client)->firstOrFail();
                    if (is_null($client->buyerEmail)) {
                        $client->buyerEmail = $request->buyerEmail;
                        $client->saveOrFail();
                    }
                    $order->id_cliente = $request->id_client;
                }
                if (!$request->credit) {
                    $order->status = 'En proceso';
                    $order->estado = 'En espera';
                }
                $pago->saveOrFail();
                $order->id_pago = $pago->id;
                $order->saveOrFail();
                return response()->json(['id' => $pago->id], 201);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pago_payu $pago_payu
     *
     * @return \Illuminate\Http\Response
     */
    public function show(pago_payu $pago_payu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pago_payu $pago_payu
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(pago_payu $pago_payu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\pago_payu $pago_payu
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pago_payu $pago_payu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pago_payu $pago_payu
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(pago_payu $pago_payu)
    {
        //
    }

    public function confirmation(Request $request)
    {
        try {
            $pago = pago_payu::where('id', $request->reference_sale)->firstOrFail();
            $order = order::where('id_pago', $pago->id)->firstOrFail();
            switch ($request->payment_method_type) {
                case 2:
                    $method = 'Tarjeta de credito';
                    break;
                case 4:
                    $method = 'Transferencia bancaria';
                    break;
                case 6:
                    $method = 'Transferencia bancaria';
                    break;
                case 7:
                    $method = 'Efectivo';
                    break;
                default:
                    $method = $request->payment_method_type;
                    break;
            }
            if ($request->state_pol == 4) {
                $order->estado = 'Aprobado';
                $order->method = $method;
                $order->status = 'Confirmado';
                $order->entidad = $request->payment_method_name;
                $order->saveOrFail();
            } else if ($request->state_pol == 6) {
                $order->estado = 'Rechazado';
                $order->method = $method;
                $order->entidad = $request->payment_method_name;
                $order->saveOrFail();
            } else if ($request->state_pol == 104) {
                $order->estado = 'Rechazado';
                $order->method = $method;
                $order->entidad = $request->payment_method_name;
                $order->saveOrFail();
            } else if ($request->state_pol == 7) {
                $order->estado = 'En espera';
                $order->method = $method;
                $order->entidad = $request->payment_method_name;
                $order->saveOrFail();
            }
            $products = [];
            $currency = 'COP';
            $data['frixionista'] = false;
            $porc = 1.04;
            if (!is_null($order->id_cliente)) {
                $client = client::where('id', $order->id_cliente)->firstOrFail();
                $currency = $client->frixionista ? 'MAY' : 'COP';
                $data['frixionista'] = $client->frixionista;
                $porc = !$client->frixionista ? 1.04 : 1;
            }

            if (strlen($order->products) > 1) {
                $arr = explode(',', $order->products);
                $arrzize = explode(',', $order->size);
                $arrquantity = explode(',', $order->quantity);
                $order->products = [];
                foreach ($arr as $index2 => $item2) {
                    $tmp = product::where('id', $item2)->firstOrFail();
                    $tmp->size = $arrzize[$index2];
                    $tmp->quantity = $arrquantity[$index2];
                    $price = DB::table('pivot_prices')
                        ->join('prices', 'prices.id', '=', 'pivot_prices.id_price')
                        ->where('pivot_prices.id_product', $tmp->id)
                        ->where('prices.type_money', $currency)
                        ->select('pivot_prices.price')
                        ->get();
                    $tmp->price = $price[0]->price;
                    $products[] = $tmp;
                }
            } else {
                $tmp = product::where('id', $order->products)->firstOrFail();
                $tmp->size = $order->size;
                $tmp->quantity = $order->quantity;
                $price = DB::table('pivot_prices')
                    ->join('prices', 'prices.id', '=', 'pivot_prices.id_price')
                    ->where('pivot_prices.id_product', $tmp->id)
                    ->where('prices.type_money', $currency)
                    ->select('pivot_prices.price')
                    ->get();
                $tmp->price = $price[0]->price;
                $products[] = $tmp;
            }
            foreach ($products as $item) {
                $data['products'][] = ['referencia' => $item->shortDetails, 'cantidad' => $item->quantity, 'total' => $item->price, 'talla' => $item->size];
            }
            $data['dtco'] = $pago->dtco;
            $pdf = PDF::loadView('pdf', compact('data'))->save('../files/' . $order->id . '.pdf');
            $this->mailer('Estado compra FRIXIO', $pago->buyerEmail, 'El estado de tu compra se encuentra ' . $order->status . ' Revisa el archivo adjunto para ver las prendas compradas' . '\n' . 'Direcion: ' . $pago->shippingAddress . '\n' . 'Ciudad: ' . $pago->shippingCity . '\n' . 'Departamento: ' . $pago->state . '\n' . 'Cedula: ' . $order->cedula, '../files/' . $order->id . '.pdf');
            $this->mailer($pago->buyerFullName, 'info@bordadosdecartago.com', 'Compra realizada por ' . $pago->buyerFullName . ' por un total de $' . $pago->amount, '../files/' . $order->id . '.pdf');
        } catch (ModelNotFoundException $exception) {
            return response($exception, 500);
        }
    }

    public function transacion(Request $request)
    {
        $datos = base64_decode($request->data);
        $datos = json_decode($datos);
        $data['tipo'] = $datos->tipo;
        $data['imagen'] = $datos->imagen;
        $data['total'] = $datos->total;
        $data['dtco'] = $datos->descuento;
        $data['envio'] = $datos->envio;
        $data['cedula'] = $datos->cedula;
        foreach ($datos->products as $index => $p) {
            $data['products'][$index]['ref'] = $p->ref;
            $data['products'][$index]['quantity'] = $p->quantity;
            $data['products'][$index]['precio'] = $p->precio;
        }
        $pdf = PDF::loadView('descarga', compact('data'));
        $pdf->save('../files/' . $request->id . '.pdf');
        $this->mailer('Estado compra FRIXIO', $request->email, 'El estado de tu compra se encuentra ' . 'en proceso de compra' . ' Revisa el archivo adjunto para ver las prendas separadas', '../files/' . $request->id . '.pdf');
//        $this->mailer($request->name, 'info@bordadosdecartago.com', 'Compra realizada por ' . $request->name . ' por un total de $' . $datos->total, '../files/' . $request->id . '.pdf');
        return response('ok', 200);
    }

    public function payu(Request $request)
    {
        try {
            $data = pago_payu::where('id', $request->id)->firstOrFail();
            return response($data, 200);
        } catch (ModelNotFoundException $exception) {
            return response($exception->getMessage(), 500);
        }
    }

    private function mailer(string $subject, string $email, string $estado, string $path)
    {
        $transport = (new Swift_SmtpTransport('bordadosdecartago.com', 465, 'ssl'))
            ->setUsername('info@bordadosdecartago.com')
            ->setPassword('s4srU4PJKA&i');
        $mailer = new Swift_Mailer($transport);
        $message = (new Swift_Message())
            ->setSubject($subject)
            ->setFrom(['ventas@frixiohechoamano.com' => 'FRIXIO'])
            ->setTo($email)
            ->setBody($estado)
            ->attach(\Swift_Attachment::fromPath($path));
//            ->addPart($html, 'text/html');
        return $mailer->send($message);
    }

    public function cash(Request $request)
    {
        $datos = base64_decode($request->data);
        $datos = json_decode($datos);
        $data['tipo'] = $datos->tipo;
        $data['imagen'] = $datos->imagen;
        $data['total'] = $datos->total;
        $data['dtco'] = $datos->descuento;
        $data['envio'] = $datos->envio;
        $data['cedula'] = $datos->cedula;
        foreach ($datos->products as $index => $p) {
            $data['products'][$index]['ref'] = $p->ref;
            $data['products'][$index]['quantity'] = $p->quantity;
            $data['products'][$index]['precio'] = $p->precio;
        }
        $pdf = PDF::loadView('descarga', compact('data'));
        $pdf->save('../files/' . $request->id . '.pdf');
        $this->mailer('Estado compra FRIXIO', $request->email, 'El estado de tu compra se encuentra ' . 'en proceso de compra' . ' Revisa el archivo adjunto para ver las prendas separadas', '../files/' . $request->id . '.pdf');
//        $this->mailer($request->name, 'info@bordadosdecartago.com', 'Compra realizada por ' . $request->name . ' por un total de $' . $datos->total, '../files/' . $request->id . '.pdf');
        return $pdf->stream();
    }
}
