<?php

namespace App\Http\Controllers;

use App\collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollectionController extends
    Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isJson()) {
            $colection = collection::all();
            $tmp = [];
            foreach ($colection as $index => $collection) {
                $tProduct = DB::table('products')->where('id_collection', $collection->id)->where('new', false)->count();
                $tmp[$index] = $collection;
                $tmp[$index]['productos'] = $tProduct;
            }
            $arr = [];
            foreach ($colection as $index => $collection) {
                if ($collection['productos'] !== 0) {
                    $arr[] = $collection;
                }
            }
            return response()->json($arr, 200);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isJson()) {
            try {
                $collection = new collection();
                $collection->fill($request->all());
                $collection->saveOrFail();
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\collection $collection
     *
     * @return \Illuminate\Http\Response
     */
    public function show(collection $collection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\collection $collection
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(collection $collection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\collection $collection
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, collection $collection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\collection $collection
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(collection $collection)
    {
        //
    }
}
