<?php

namespace App\Http\Controllers;

use App\pivot_price;
use Illuminate\Http\Request;

class PivotPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pivot_price  $pivot_price
     * @return \Illuminate\Http\Response
     */
    public function show(pivot_price $pivot_price)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pivot_price  $pivot_price
     * @return \Illuminate\Http\Response
     */
    public function edit(pivot_price $pivot_price)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pivot_price  $pivot_price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pivot_price $pivot_price)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pivot_price  $pivot_price
     * @return \Illuminate\Http\Response
     */
    public function destroy(pivot_price $pivot_price)
    {
        //
    }
}
