<?php

namespace App\Http\Controllers;

use App\client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class ClientController extends
    Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function show(client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, client $client)
    {
        try {
            $client->fill($request->all());
            $client->saveOrFail();
            return response()->json(['cliente' => $client, 'frixionista' => $client->frixionista]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => $exception->getMessage()], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(client $client)
    {
        //
    }

    public function excel(Request $request)
    {
        try {
            if ($request->hasFile('excel')) {
                Excel::load($request->file('excel'), function ($reader) {
                    $result = $reader->get();
                    foreach ($result as $item) {
                        $client = new client();
                        $client->buyerFullName = $item->buyerfullname;
                        $client->identification = $item->identification;
                        $client->shippingAddress = $item->shippingaddress;
                        $client->telephone = $item->telephone;
                        $client->state = $item->state;
                        $client->shippingCountry = 'CO';
                        $client->shippingCity = $item->shippingcity;
                        $client->password = Hash::make($item->identification);
                        $client->frixionista = true;
                        $client->saveOrFail();
                    }
                });
            }
            return response()->json(['message' => 'ok'], 201);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function authFrixionista(Request $request)
    {
        try {
//            if (filter_var($request->nombre, FILTER_VALIDATE_EMAIL)) {
//                $client = where('buyerEmail', $request->nombre)->firstOrFail();
//                if (Hash::check($request->cedula, $client->password)) {
//                    return response()->json(['cliente' => $client, 'frixionista' => $client->frixionista], 200);
//                }
//                return response()->json(['message' => 'Datos incorrectos'], 500);
//            } else {
            $client = client::where('identification', $request->nombre)->firstOrFail();
            if (Hash::check($request->cedula, $client->password)) {
                return response()->json(['cliente' => $client, 'frixionista' => $client->frixionista], 200);
            }
            return response()->json(['message' => 'Datos incorrectos'], 500);

//            }
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Datos incorrectos'], 500);
        }

    }
}
