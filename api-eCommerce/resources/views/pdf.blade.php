@php
    $total=0;
    $cantidad=0;
    $envio=0;
    foreach ($data['products'] as $item){
        $total+=$item['total']*$item['cantidad'];
        $cantidad+=$item['cantidad'];
    }
    if ($cantidad<4){
        $envio=6000;
    }
$dtcoG=0.75;
    if($cantidad<7){
           $dtcoG=0.75;
    }else{
            $dtcoG=0.65;
    }
    function obtenerFechaEnLetra($fecha){
        $dia= conocerDiaSemanaFecha($fecha);
        $num = date("j", strtotime($fecha));
        $anno = date("Y", strtotime($fecha));
        $mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        $mes = $mes[(date('m', strtotime($fecha))*1)-1];
        return $dia.', '.$num.' de '.$mes.' del '.$anno;
    }
    function conocerDiaSemanaFecha($fecha) {
        $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
        $dia = $dias[date('w', strtotime($fecha))];
        return $dia;
    }
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '+15 day' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
        $nuevafecha=obtenerFechaEnLetra($nuevafecha);
@endphp
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Frixio</title>
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<style>
    .checkout-details {
        position: relative;
        background-color: #f9f9f9;
        border: 1px solid #dddddd;
        padding: 40px;
    }

    .order-box {
        padding-bottom: 25px;
        color: #444444;
        font-weight: 600;
        font-size: 22px;
        border-bottom: 1px solid #ededed;
        position: relative;
        margin-bottom: 50px;
        width: 100vw;
        /*float: right;*/
        /*line-height: 1.2em;*/
    }

    .order-box .title-box span {
        position: relative;
        width: 35%;
        float: right;
        line-height: 1.2em;
    }

    .order-box .qty {
        position: relative;
        border-bottom: 1px solid #ededed;
        margin-bottom: 30px;
    }

    .order-box .qty li {
        position: relative;
        display: block;
        font-size: 15px;
        color: #444444;
        line-height: 20px;
        margin-bottom: 20px;
    }

    .order-box .qty li span {
        float: right;
        font-size: 18px;
        line-height: 20px;
        color: #232323;
        font-weight: 400;
        width: 35%;
    }

    .order-box .sub-total {
        position: relative;
        border-bottom: 1px solid #dddddd;
        margin-bottom: 30px;
    }

    .order-box .sub-total li {
        position: relative;
        font-size: 16px;
        font-weight: 600;
        color: #333333;
        line-height: 20px;
        margin-bottom: 20px;
        width: 100vw;
    }

    .order-box .sub-total li .count {
        position: relative;
        font-size: 18px;
        line-height: 20px;
        color: blue;
        font-weight: 400;
        width: 35%;
        float: right;
    }

    .order-box .sub-total .shopping-option label {
        position: relative;
        font-size: 16px;
        line-height: 32px;
        padding-left: 10px;
        color: #6f6f6f;
    }

    .order-box .sub-total .shipping {
        width: 35%;
        float: right;
    }

    .order-box .total {
        position: relative;
        margin-bottom: 40px;
    }

    .order-box .total li {
        position: relative;
        display: block;
        font-weight: 400;
        color: #333333;
        line-height: 20px;
        margin-bottom: 10px;
        font-size: 18px;
    }

    .order-box .total li .count {
        position: relative;
        font-size: 24px;
        line-height: 20px;
        color: blue;
        font-weight: 400;
    }

    .order-box .total li span {
        float: right;
        font-size: 15px;
        line-height: 20px;
        color: #444444;
        font-weight: 400;
        width: 35%;
        display: block;
    }
</style>
<img src="../images/logo.png" alt="" class="img-fluid">
<div class="modal-header d-flex  flex-wrap">
    <h4 class="modal-title" id="modal-basic-title">Resumen de compra</h4>
</div>

<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="col-10">
                <div class="checkout-details">
                    <div class="order-box">
                        <div class="title-box">
                            <div>Producto <span> Total</span></div>
                        </div>
                        <ul class="qty">
                            @foreach($data['products'] as $product)

                                <li>
                                    {{$product['referencia'] }}x{{ $product['cantidad']}}
                                    <span>$ {{($product['total']*$product['cantidad'])*$dtcoG}}</span>
                                </li>
                            @endforeach
                        </ul>
                        <ul class="sub-total">
                            <li>
                                Envío<span
                                    class="count">$ {{$envio}}</span>
                            </li>
                            <li>
                                Descuento<span
                                    class="count">$ {{$data['dtco']}}</span>
                            </li>
                        </ul>
                        <ul class="total">
                            <li><h2>Total <span
                                        class="count">$ {{($total*$dtcoG)+$envio}}</span></h2>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <h3>Fecha aproximada de envio: <span>{{$nuevafecha}}</span></h3>
</div>

</body>
</html>
