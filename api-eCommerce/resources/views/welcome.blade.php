<!doctype html>
<?php
$id = $_GET['id'];
$data = http_build_query(array('id' => $id));
$opciones = array('http' => array(
    'method' => 'POST',
    'header' => 'Content-type: application/x-www-form-urlencoded',
    'content' => $data
));
$contexto = stream_context_create($opciones);
$resultado = json_decode(file_get_contents('https://api.frixiohechoamano.com/api-eCommerce/public/index.php/api/payu', true,
    $contexto));
$signature = md5('5tfei8udic3kr57i1ag9vku483~501278~' . $resultado->id . '~' . $resultado->amount . '~' . $resultado->currency . '~' . $resultado->shippingValue);
?>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
</head>
<body>
<div>
    <form method="post" action="https://checkout.payulatam.com/ppp-web-gateway-payu/">
        <input name="merchantId" type="hidden" value="501278">
        <input name="accountId" type="hidden" value="502068">
        <input name="description" type="hidden" value="{{$resultado->description}}">
        <input name="referenceCode" type="hidden" value="{{$resultado->id}}">
        <input name="amount" type="hidden" value="{{$resultado->amount}}">
        <input name="tax" type="hidden" value="{{$resultado->tax}}">
        <input name="taxReturnBase" type="hidden" value="{{$resultado->taxReturnBase}}">
        <input name="currency" type="hidden" value="{{$resultado->currency}}">
        <input name="signature" type="hidden" value="{{$signature}}">
        <input name="buyerEmail" type="hidden" value="{{$resultado->buyerEmail}}">
        <input name="buyerFullName" type="hidden" value="{{$resultado->buyerFullName}}">
        <input name="responseUrl" type="hidden"
               value="https://api.frixiohechoamano.com/api-eCommerce/public/index.php/response">
        <input name="confirmationUrl" type="hidden"
               value="https://api.frixiohechoamano.com/api-eCommerce/public/index.php/api/confirmation">
        <input name="shippingAddress" type="hidden" value="{{$resultado->shippingAddress}}">
        <input name="shippingCity" type="hidden" value="{{$resultado->shippingCity}}">
        <input name="shippingValue" type="hidden" value="{{$resultado->shippingValue}}">
        <input name="shippingCountry" type="hidden" value="{{$resultado->shippingCountry}}">
        <input name="telephone" type="hidden" value="{{$resultado->telephone}}">
        <input name="Submit" type="submit" id="button" value="Enviar" style="opacity: 0">
    </form>
</div>
<script>

    let button = document.getElementById('button');
    button.click();
</script>
</body>
</html>
