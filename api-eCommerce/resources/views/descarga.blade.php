@php
    setlocale(LC_MONETARY,'en_US.UTF-8');
    function obtenerFechaEnLetra($fecha){
        $dia= conocerDiaSemanaFecha($fecha);
        $num = date("j", strtotime($fecha));
        $anno = date("Y", strtotime($fecha));
        $mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        $mes = $mes[(date('m', strtotime($fecha))*1)-1];
        return $dia.', '.$num.' de '.$mes.' del '.$anno;
    }
    function conocerDiaSemanaFecha($fecha) {
        $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
        $dia = $dias[date('w', strtotime($fecha))];
        return $dia;
    }
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '+5 day' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
        $nuevafecha=obtenerFechaEnLetra($nuevafecha);
@endphp
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Frixio</title>
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<style>
    .checkout-details {
        position: relative;
        background-color: #f9f9f9;
        border: 1px solid #dddddd;
        padding: 40px;
    }

    .order-box {
        padding-bottom: 25px;
        color: #444444;
        font-weight: 600;
        font-size: 22px;
        border-bottom: 1px solid #ededed;
        position: relative;
        margin-bottom: 50px;
        width: 100vw;
        /*float: right;*/
        /*line-height: 1.2em;*/
    }

    .order-box .title-box span {
        position: relative;
        width: 35%;
        float: right;
        line-height: 1.2em;
    }

    .order-box .qty {
        position: relative;
        border-bottom: 1px solid #ededed;
        margin-bottom: 30px;
    }

    .order-box .qty li {
        position: relative;
        display: block;
        font-size: 15px;
        color: #444444;
        line-height: 20px;
        margin-bottom: 20px;
    }

    .order-box .qty li span {
        float: right;
        font-size: 18px;
        line-height: 20px;
        color: #232323;
        font-weight: 400;
        width: 35%;
    }

    .order-box .sub-total {
        position: relative;
        border-bottom: 1px solid #dddddd;
        margin-bottom: 30px;
    }

    .order-box .sub-total li {
        position: relative;
        font-size: 16px;
        font-weight: 600;
        color: #333333;
        line-height: 20px;
        margin-bottom: 20px;
        width: 100vw;
    }

    .order-box .sub-total li .count {
        position: relative;
        font-size: 18px;
        line-height: 20px;
        color: blue;
        font-weight: 400;
        width: 35%;
        float: right;
    }

    .order-box .sub-total .shopping-option label {
        position: relative;
        font-size: 16px;
        line-height: 32px;
        padding-left: 10px;
        color: #6f6f6f;
    }

    .order-box .sub-total .shipping {
        width: 35%;
        float: right;
    }

    .order-box .total {
        position: relative;
    }

    .order-box .total li {
        position: relative;
        display: block;
        font-weight: 400;
        color: #333333;
        line-height: 20px;
        font-size: 18px;
    }

    .order-box .total li .count {
        position: relative;
        font-size: 24px;
        line-height: 20px;
        color: blue;
        font-weight: 400;
    }

    .order-box .total li span {
        float: right;
        font-size: 15px;
        line-height: 20px;
        color: #444444;
        font-weight: 400;
        width: 35%;
        display: block;
    }
    .order-box .total li h4 {
        font-size: 24px;
        line-height: 20px;
        color: #444444;
        font-weight: 400;
        width: 100%;
        display: block;
    }
</style>
<img src="../images/logo.png" alt="" class="img-fluid">
<div class="modal-header d-flex  flex-wrap">
    <h4 class="modal-title" id="modal-basic-title">Pago en {{$data['tipo']}}</h4>
    <img src="{{$data['imagen']}}" alt="" style="position: absolute;  right: 30%; width: 117px;height: 45px"
         class="img-fluid">
</div>
<div class="modal-body">
    @if ($data['tipo']==='Efecty')
        <h6>Pago empresarial a nombre de: <strong>Casa del bordado</strong></h6>
        <table class="table table-borderless">
            <thead>
            <tr>
                <th scope="col">Código convenio</th>
                <th scope="col">Número de pago</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"><h3><strong>51501</strong></h3></th>
                <td colspan="2"><h3><strong>{{$data['cedula']}}</strong></h3></td>
            </tr>
            </tbody>
        </table>

    @elseif($data['tipo']==='Baloto')
        <h6>Pago a nombre de: <strong>Carlos
                Alberto Perea Cardona</strong></h6>
        <table class="table table-borderless">
            <thead>
            <tr>
                <th scope="col">Convenio</th>
                <th scope="col">cuenta corriente en colpatria N°</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"><h3><strong>#955150</strong></h3></th>
                <td colspan="2"><h3><strong>8851000950</strong></h3></td>
            </tr>
            </tbody>
        </table>
    @elseif($data['tipo']==='Colpatria')
        <table class="table table-borderless">
            <thead>
            <tr>
                <th scope="col">Cuenta corriente N°</th>
                <th scope="col">A nombre de:</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"><h3><strong>8851000950</strong></h3></th>
                <td colspan="2"><h3><strong>Carlos Alberto Perea Cardona</strong></h3></td>
            </tr>
            </tbody>
        </table>
    @elseif($data['tipo']==='Bancolombia')
        <table class="table table-borderless">
            <thead>
            <tr>
                <th scope="col">Cuenta de ahorros N°</th>
                <th scope="col">A nombre de:</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"><h3><strong>72812003471</strong></h3></th>
                <td colspan="2"><h3><strong>Carlos Alberto Perea Cardona</strong></h3></td>
            </tr>
            </tbody>
        </table>
    @elseif($data['tipo']==='Davivienda')
        <table class="table table-borderless">
            <thead>
            <tr>
                <th scope="col">Cuenta de ahorros N°</th>
                <th scope="col">A nombre de:</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"><h3><strong>0000144557402</strong></h3></th>
                <td colspan="2"><h3><strong>Carlos Alberto Perea Cardona</strong></h3></td>
            </tr>
            </tbody>
        </table>
    @endif
    <h6><strong>Recuerda enviarnos el comprobante de consignacion al <a href="">3104203383</a></strong> o al <a
            href=""><strong>3137402856</strong></a></h6>
    <div class="container">
        <div class="row">
            <div class="col-10">
                <div class="checkout-details">
                    <div class="order-box">
                        <div class="title-box">
                            <div>Producto <span> Total</span></div>
                        </div>
                        <ul class="qty">
                            @foreach($data['products'] as $product)

                                <li>
                                    {{$product['ref'] }}x{{ $product['quantity']}}
                                    <span> {{money_format('%.2n',($product['precio']*$product['quantity']))}}</span>
                                </li>
                            @endforeach
                        </ul>
                        <ul class="sub-total">
                            <li>
                                Envío<span
                                    class="count">{{money_format('%.2n',$data['envio'])}}</span>
                            </li>
                            <li>
                                Descuento<span
                                    class="count"> {{money_format('%.2n',$data['dtco'])}}</span>
                            </li>
                        </ul>
                        <ul class="total">
                            <li><h4>Total <span
                                        class="count">{{money_format('%.2n',$data['total'])}}</span></h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <h3>Paga antes de: <span>{{$nuevafecha}}</span></h3>
</div>
</body>
</html>
