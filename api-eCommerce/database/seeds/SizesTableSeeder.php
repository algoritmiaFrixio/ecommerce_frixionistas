<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            'name' => '4'
        ]);
        DB::table('sizes')->insert([
            'name' => '6'
        ]);
        DB::table('sizes')->insert([
            'name' => '8'
        ]);
        DB::table('sizes')->insert([
            'name' => '10'
        ]);
        DB::table('sizes')->insert([
            'name' => '12'
        ]);
        DB::table('sizes')->insert([
            'name' => '14'
        ]);
        DB::table('sizes')->insert([
            'name' => '16'
        ]);
        DB::table('sizes')->insert([
            'name' => '18'
        ]);
        DB::table('sizes')->insert([
            'name' => '20'
        ]);
        DB::table('sizes')->insert([
            'name' => 'S'
        ]);
        DB::table('sizes')->insert([
            'name' => 'M'
        ]);
        DB::table('sizes')->insert([
            'name' => 'L'
        ]);
        DB::table('sizes')->insert([
            'name' => 'XL'
        ]);
        DB::table('sizes')->insert([
            'name' => 'XXL'
        ]);
        DB::table('sizes')->insert([
            'name' => '3XL'
        ]);
        DB::table('sizes')->insert([
            'name' => '4XL'
        ]);
        DB::table('sizes')->insert([
            'name' => '0'
        ]);
    }
}
