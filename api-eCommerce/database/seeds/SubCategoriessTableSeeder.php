<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubCategoriessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_categories')->insert([
            'name' => 'blusas',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'vestidos',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'kaflan',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'faldas',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'pantalones',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'shorts',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'mantas tipicas',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'vestidos de una pieza',
            'id_category' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'guayabera slim-fit',
            'id_category' => 2
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'guayabera manga larga',
            'id_category' => 2
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'guayabera manga corta',
            'id_category' => 2
        ]);
//        DB::table('sub_categories')->insert([
//            'name' => 'Shirt with cufflinks',
//            'id_category' => 2
//        ]);
        DB::table('sub_categories')->insert([
            'name' => 'pantalones',
            'id_category' => 2
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'bermudas',
            'id_category' => 2
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'pañuelos',
            'id_category' => 2
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'guayabera',
            'id_category' => 3
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'pantalones',
            'id_category' => 3
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'vestidos primera comunión',
            'id_category' => 4
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'prendas clériman',
            'id_category' => 5
        ]);
    }
}
