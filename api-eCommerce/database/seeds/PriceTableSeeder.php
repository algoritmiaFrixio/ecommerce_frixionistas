<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prices')->insert([
            'type_money' => 'USD'
        ]);
        DB::table('prices')->insert([
            'type_money' => 'EUR'
        ]);
        DB::table('prices')->insert([
            'type_money' => 'COP'
        ]);
        DB::table('prices')->insert([
            'type_money' => 'MAY'
        ]);
    }
}
