<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'mujer'
        ]);
        DB::table('categories')->insert([
            'name' => 'hombre'
        ]);
        DB::table('categories')->insert([
            'name' => 'niños'
        ]);
        DB::table('categories')->insert([
            'name' => 'niñas'
        ]);
        DB::table('categories')->insert([
            'name' => 'especial'
        ]);
    }
}
