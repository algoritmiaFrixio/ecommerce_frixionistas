<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CollectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('collections')->insert([
            'name' => 'hecho a mano',
            'img' => 'http://api.frixiohechoamano.com/ecommerce_frixionistas/api-eCommerce/images/collection/hechoamano/d6874c95-b8e7-466b-bf90-38a43bdd1e92.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'hecho a mano masculino',
            'img' => 'http://api.frixiohechoamano.com/ecommerce_frixionistas/api-eCommerce/images/collection/hechoamano/d6874c95-b8e7-466b-bf90-38a43bdd1e92.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'enamora',
            'img' => 'http://api.frixiohechoamano.com/ecommerce_frixionistas/api-eCommerce/images/collection/enamora/ee3220c0-be5e-439a-81e8-6bee7d06f697.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'florecer',
            'img' => 'http://api.frixiohechoamano.com/ecommerce_frixionistas/api-eCommerce/images/collection/florecer/3cce66bb-4832-4ff9-8e91-c2ccf7d82cad.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'arte y moda',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/arteymoda/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'bordando arte',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/bordandoarte/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'caribe masculino',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/caribemasculino/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'caribe',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/caribemujer/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'caribe plus',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/caribeplus/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'creando tendencias',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/creandotendencias/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'creando tendencias masculino',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/creandotendenciasmasculino/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'tradicion y moda',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/tradicionymoda/portada2.jpg'
        ]);
        DB::table('collections')->insert([
            'name' => 'tradicion y moda plus',
            'img' => 'http://api.frixiohechoamano.com/api-eCommerce/images/collection/tradicionymodaplus/portada2.jpg'
        ]);
    }
}
