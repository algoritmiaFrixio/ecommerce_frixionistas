<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagoPayusTable extends
    Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago_payus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 50);
            $table->string('amount', 50);
            $table->string('tax', 50);
            $table->string('taxReturnBase', 50);
            $table->string('currency', 50);
            $table->string('buyerEmail', 50);
            $table->string('buyerFullName', 50);
            $table->string('dtco', 50);
            $table->string('shippingAddress', 50);
            $table->string('shippingCity', 50);
            $table->string('shippingCountry', 50);
            $table->string('shippingValue', 50);
            $table->string('telephone', 50);
            $table->string('state', 50);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago_payus');
    }
}
