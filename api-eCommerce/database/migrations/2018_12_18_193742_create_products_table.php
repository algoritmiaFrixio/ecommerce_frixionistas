<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->float('discount')->unsigned()->default(0);
            $table->string('shortDetails', 25)->unique();
            $table->string('colorSpanish', 200)->nullable();
            $table->string('references')->nullable();
            $table->string('tela', 100);
            $table->string('tecnica', 100);
            $table->text('description');
            $table->integer('stock')->unsigned();
            $table->boolean('new')->default(true);
            $table->boolean('sale')->default(true);
            $table->unsignedInteger('id_subcategory')->nullable();
            $table->unsignedInteger('id_collection')->nullable();
            $table->timestamps();
            $table->foreign('id_subcategory')->references('id')->on('sub_categories');
            $table->foreign('id_collection')->references('id')->on('collections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
