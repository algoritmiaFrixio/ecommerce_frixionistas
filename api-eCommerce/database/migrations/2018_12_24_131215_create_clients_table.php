<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identification', 50);
            $table->string('buyerEmail', 100)->unique()->nullable();
            $table->string('password', 60);
            $table->string('buyerFullName', 100);
            $table->string('telephone', 25)->nullable();
            $table->string('shippingCity', 50);
            $table->string('state', 50);
            $table->string('shippingCountry', 50);
            $table->string('postalcode', 25)->nullable();
            $table->string('shippingAddress', 100);
            $table->boolean('frixionista')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
