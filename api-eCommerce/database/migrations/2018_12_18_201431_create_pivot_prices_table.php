<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_price');
            $table->unsignedInteger('id_product');
            $table->float('price');
            $table->timestamps();

            $table->foreign('id_price')->references('id')->on('prices');
            $table->foreign('id_product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_prices');
    }
}
