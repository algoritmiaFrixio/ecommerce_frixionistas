<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('products');
            $table->string('quantity');
            $table->string('size');
            $table->string('cedula');
            $table->unsignedInteger('id_cliente')->nullable();
            $table->unsignedInteger('id_pago');
            $table->enum('status', ['En proceso', 'Confirmado', 'En camino', 'Entregado'])->default('En proceso');
            $table->enum('estado', ['Aprobado', 'Rechazado', 'En espera'])->nullable();
            $table->string('method', 50)->nullable();
            $table->string('entidad', 50)->nullable();
            $table->timestamps();
            $table->foreign('id_cliente')->references('id')->on('clients');
            $table->foreign('id_pago')->references('id')->on('pago_payus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
