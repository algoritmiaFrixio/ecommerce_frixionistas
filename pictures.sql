﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-02-25 15:41:50

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 215 (class 1259 OID 18951)
-- Name: pictures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pictures (
    id integer NOT NULL,
    path character varying(255) NOT NULL,
    id_color integer,
    id_product integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pictures OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 18949)
-- Name: pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pictures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pictures_id_seq OWNER TO postgres;

--
-- TOC entry 2891 (class 0 OID 0)
-- Dependencies: 214
-- Name: pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pictures_id_seq OWNED BY public.pictures.id;


--
-- TOC entry 2758 (class 2604 OID 19058)
-- Name: pictures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures ALTER COLUMN id SET DEFAULT nextval('public.pictures_id_seq'::regclass);


--
-- TOC entry 2885 (class 0 OID 18951)
-- Dependencies: 215
-- Data for Name: pictures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pictures (id, path, id_color, id_product, created_at, updated_at) FROM stdin;
1	https://api.frixiohechoamano.com/api-eCommerce/images/product/3/154670353501778.jpg	1	3	2019-01-05 15:52:15	2019-01-05 15:52:15
2	https://api.frixiohechoamano.com/api-eCommerce/images/product/4/154670387001779.jpg	3	4	2019-01-05 15:57:50	2019-01-05 15:57:50
3	https://api.frixiohechoamano.com/api-eCommerce/images/product/5/154670427101780.jpg	4	5	2019-01-05 16:04:31	2019-01-05 16:04:31
4	https://api.frixiohechoamano.com/api-eCommerce/images/product/5/154670427101780-2.jpg	\N	5	2019-01-05 16:04:31	2019-01-05 16:04:31
5	https://api.frixiohechoamano.com/api-eCommerce/images/product/6/154670496401781.jpg	1	6	2019-01-05 16:16:04	2019-01-05 16:16:04
6	https://api.frixiohechoamano.com/api-eCommerce/images/product/7/154670535701782.jpg	1	7	2019-01-05 16:22:37	2019-01-05 16:22:37
7	https://api.frixiohechoamano.com/api-eCommerce/images/product/8/154670572801783.jpg	7	8	2019-01-05 16:28:48	2019-01-05 16:28:48
8	https://api.frixiohechoamano.com/api-eCommerce/images/product/9/154670606801784.jpg	1	9	2019-01-05 16:34:28	2019-01-05 16:34:28
9	https://api.frixiohechoamano.com/api-eCommerce/images/product/9/154670606801784-2.jpg	\N	9	2019-01-05 16:34:28	2019-01-05 16:34:28
10	https://api.frixiohechoamano.com/api-eCommerce/images/product/10/154670633401785.jpg	1	10	2019-01-05 16:38:54	2019-01-05 16:38:54
11	https://api.frixiohechoamano.com/api-eCommerce/images/product/10/154670633401785-2.jpg	\N	10	2019-01-05 16:38:54	2019-01-05 16:38:54
12	https://api.frixiohechoamano.com/api-eCommerce/images/product/11/154670671001785P.jpg	1	11	2019-01-05 16:45:10	2019-01-05 16:45:10
13	https://api.frixiohechoamano.com/api-eCommerce/images/product/12/154670694401786.jpg	\N	12	2019-01-05 16:49:04	2019-01-05 16:49:04
14	https://api.frixiohechoamano.com/api-eCommerce/images/product/12/154670694401786-2.jpg	\N	12	2019-01-05 16:49:04	2019-01-05 16:49:04
15	https://api.frixiohechoamano.com/api-eCommerce/images/product/13/154670747501-787 (1).jpg	10	13	2019-01-05 16:57:55	2019-01-05 16:57:55
16	https://api.frixiohechoamano.com/api-eCommerce/images/product/13/154670747501-787 (2).jpg	\N	13	2019-01-05 16:57:55	2019-01-05 16:57:55
17	https://api.frixiohechoamano.com/api-eCommerce/images/product/14/154670860701-787 (3).jpg	10	14	2019-01-05 17:16:47	2019-01-05 17:16:47
18	https://api.frixiohechoamano.com/api-eCommerce/images/product/15/154670886401788.jpg	3	15	2019-01-05 17:21:04	2019-01-05 17:21:04
19	https://api.frixiohechoamano.com/api-eCommerce/images/product/16/154695569301789.jpg	1	16	2019-01-08 13:54:53	2019-01-08 13:54:53
20	https://api.frixiohechoamano.com/api-eCommerce/images/product/17/154695595305288.jpg	\N	17	2019-01-08 13:59:13	2019-01-08 13:59:13
21	https://api.frixiohechoamano.com/api-eCommerce/images/product/17/154695595305288-2.jpg	2	17	2019-01-08 13:59:13	2019-01-08 13:59:13
22	https://api.frixiohechoamano.com/api-eCommerce/images/product/18/154697636001790.jpg	7	18	2019-01-08 19:39:20	2019-01-08 19:39:20
23	https://api.frixiohechoamano.com/api-eCommerce/images/product/18/154697636001790-2.jpg	\N	18	2019-01-08 19:39:20	2019-01-08 19:39:20
24	https://api.frixiohechoamano.com/api-eCommerce/images/product/19/154697691101791.jpg	1	19	2019-01-08 19:48:31	2019-01-08 19:48:31
25	https://api.frixiohechoamano.com/api-eCommerce/images/product/20/154697726201792.jpg	10	20	2019-01-08 19:54:22	2019-01-08 19:54:22
26	https://api.frixiohechoamano.com/api-eCommerce/images/product/20/154697726201792-2.jpg	\N	20	2019-01-08 19:54:22	2019-01-08 19:54:22
27	https://api.frixiohechoamano.com/api-eCommerce/images/product/20/154697726201792-3.jpg	\N	20	2019-01-08 19:54:22	2019-01-08 19:54:22
28	https://api.frixiohechoamano.com/api-eCommerce/images/product/21/154697754701792-2.jpg	\N	21	2019-01-08 19:59:07	2019-01-08 19:59:07
29	https://api.frixiohechoamano.com/api-eCommerce/images/product/21/154697754701792-4.jpg	10	21	2019-01-08 19:59:07	2019-01-08 19:59:07
30	https://api.frixiohechoamano.com/api-eCommerce/images/product/21/154697754701792-5.jpg	\N	21	2019-01-08 19:59:07	2019-01-08 19:59:07
31	https://api.frixiohechoamano.com/api-eCommerce/images/product/22/154697799701793.jpg	6	22	2019-01-08 20:06:37	2019-01-08 20:06:37
32	https://api.frixiohechoamano.com/api-eCommerce/images/product/23/154697825401794.jpg	6	23	2019-01-08 20:10:54	2019-01-08 20:10:54
33	https://api.frixiohechoamano.com/api-eCommerce/images/product/24/154697892505289.jpg	6	24	2019-01-08 20:22:05	2019-01-08 20:22:05
34	https://api.frixiohechoamano.com/api-eCommerce/images/product/25/154698001308464.jpg	1	25	2019-01-08 20:40:13	2019-01-08 20:40:13
36	https://api.frixiohechoamano.com/api-eCommerce/images/product/27/154698098508466.jpg	6	27	2019-01-08 20:56:25	2019-01-08 20:56:25
37	https://api.frixiohechoamano.com/api-eCommerce/images/product/28/154698114608465.jpg	4	28	2019-01-08 20:59:06	2019-01-08 20:59:06
38	https://api.frixiohechoamano.com/api-eCommerce/images/product/29/154698143708467.jpg	6	29	2019-01-08 21:03:57	2019-01-08 21:03:57
39	https://api.frixiohechoamano.com/api-eCommerce/images/product/30/154698174708468.jpg	6	30	2019-01-08 21:09:07	2019-01-08 21:09:07
40	https://api.frixiohechoamano.com/api-eCommerce/images/product/31/154698190608469.jpg	10	31	2019-01-08 21:11:46	2019-01-08 21:11:46
41	https://api.frixiohechoamano.com/api-eCommerce/images/product/32/154698226708470.jpg	\N	32	2019-01-08 21:17:47	2019-01-08 21:17:47
42	https://api.frixiohechoamano.com/api-eCommerce/images/product/33/154698252508471.jpg	7	33	2019-01-08 21:22:05	2019-01-08 21:22:05
43	https://api.frixiohechoamano.com/api-eCommerce/images/product/34/154704311308472.jpg	1	34	2019-01-09 14:11:53	2019-01-09 14:11:53
44	https://api.frixiohechoamano.com/api-eCommerce/images/product/34/154704311308472-2.jpg	\N	34	2019-01-09 14:11:54	2019-01-09 14:11:54
45	https://api.frixiohechoamano.com/api-eCommerce/images/product/35/154704338208473.jpg	1	35	2019-01-09 14:16:22	2019-01-09 14:16:22
46	https://api.frixiohechoamano.com/api-eCommerce/images/product/36/154704388208474.jpg	10	36	2019-01-09 14:24:42	2019-01-09 14:24:42
47	https://api.frixiohechoamano.com/api-eCommerce/images/product/37/154704475408475.jpg	6	37	2019-01-09 14:39:14	2019-01-09 14:39:14
48	https://api.frixiohechoamano.com/api-eCommerce/images/product/38/154704542708476.jpg	1	38	2019-01-09 14:50:27	2019-01-09 14:50:27
49	https://api.frixiohechoamano.com/api-eCommerce/images/product/39/154704565708477.jpg	6	39	2019-01-09 14:54:17	2019-01-09 14:54:17
50	https://api.frixiohechoamano.com/api-eCommerce/images/product/40/154704671808478.jpg	1	40	2019-01-09 15:11:58	2019-01-09 15:11:58
51	https://api.frixiohechoamano.com/api-eCommerce/images/product/41/154704688208479.jpg	10	41	2019-01-09 15:14:42	2019-01-09 15:14:42
52	https://api.frixiohechoamano.com/api-eCommerce/images/product/42/154704747908480.jpg	4	42	2019-01-09 15:24:39	2019-01-09 15:24:39
53	https://api.frixiohechoamano.com/api-eCommerce/images/product/43/154704826408481.jpg	1	43	2019-01-09 15:37:44	2019-01-09 15:37:44
54	https://api.frixiohechoamano.com/api-eCommerce/images/product/43/154704826408481-2.jpg	\N	43	2019-01-09 15:37:44	2019-01-09 15:37:44
55	https://api.frixiohechoamano.com/api-eCommerce/images/product/44/154704849508481CH.jpg	\N	44	2019-01-09 15:41:35	2019-01-09 15:41:35
56	https://api.frixiohechoamano.com/api-eCommerce/images/product/44/154704849508481N.jpg	1	44	2019-01-09 15:41:35	2019-01-09 15:41:35
57	https://api.frixiohechoamano.com/api-eCommerce/images/product/45/154704895708481CH.jpg	1	45	2019-01-09 15:49:17	2019-01-09 15:49:17
58	https://api.frixiohechoamano.com/api-eCommerce/images/product/45/154704895708481CH-2.jpg	\N	45	2019-01-09 15:49:17	2019-01-09 15:49:17
59	https://api.frixiohechoamano.com/api-eCommerce/images/product/46/154704911308482.jpg	\N	46	2019-01-09 15:51:53	2019-01-09 15:51:53
60	https://api.frixiohechoamano.com/api-eCommerce/images/product/47/Sin-título-1.jpg	1	47	2019-01-09 15:51:53	2019-01-09 15:51:53
\.


--
-- TOC entry 2892 (class 0 OID 0)
-- Dependencies: 214
-- Name: pictures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pictures_id_seq', 60, true);


--
-- TOC entry 2760 (class 2606 OID 18956)
-- Name: pictures pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_pkey PRIMARY KEY (id);


--
-- TOC entry 2762 (class 2606 OID 18962)
-- Name: pictures pictures_id_color_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_id_color_foreign FOREIGN KEY (id_color) REFERENCES public.colors(id);


--
-- TOC entry 2761 (class 2606 OID 18957)
-- Name: pictures pictures_id_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_id_product_foreign FOREIGN KEY (id_product) REFERENCES public.products(id);


-- Completed on 2019-02-25 15:41:52

--
-- PostgreSQL database dump complete
--

